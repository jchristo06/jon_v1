#!/Users/jonathanchristophersen/anaconda/bin/python

import numpy as np
from scipy import stats
from scipy.stats import norm


def getarraydata(fn, minn, maxn):
    ext = ".ascii"
    ls = [fn + repr(i) + ext for i in range(minn, maxn + 1)]

    return ls


def getinitcondarraydata(fn, iminn, imaxn, jminn, jmaxn):
    ls = []
    ext = ".ascii"
    for i in range(iminn, imaxn):
        for j in range(jminn, jmaxn):
            ls.append(fn + repr(i) + "_" + repr(j) + ext)

    return ls


def getfloatdata(fn):
    with open(fn) as f:
        data = map(float, f)

    return data


def getcomplexdata(fn):
    with open(fn) as f:
        data = map(complex, f)

    return data


def gethisto(fn):
    # with open(fn) as f:
    #     data = map(float, f)
    data = np.array(getfloatdata(fn))

    print "Traversing data for NaN values ... "
    data = np.ma.masked_where(np.isnan(data), data)

    print "Getting statistics"
    a = np.ma.mean(data)
    print "Average is done ... ", a
    v = np.ma.var(data)
    print "Variance is done ... ", v
    s = stats.skew(data.compressed())
    print "Skewness is done ... ", s
    k = stats.kurtosis(data.compressed())
    print "Kurtosis is done ... ", k

    maxim = max(data.compressed())
    minim = min(data.compressed())

    print len(data.compressed())

    # Gaussian Reference
    print "Fit Normal distribution curve to the data ... "
    mu, std = norm.fit(data.compressed())
    # sumv=np.sum(SSH)
    # print sumv
    print "Building histogram information ... "
    histo, bins = np.histogram(data.compressed(), bins=20)
    return (histo, bins, mu, std, a, v, s, k, maxim, minim)


def getstatistics(data):
    # Calling to get histogram and statistical information with just an array
    print "Getting statistics"
    a = np.ma.mean(data)
    print "Average is done ... ", a
    v = np.ma.var(data)
    print "Variance is done ... ", v
    s = stats.skew(data.compressed())
    print "Skewness is done ... ", s
    k = stats.kurtosis(data.compressed())
    print "Kurtosis is done ... ", k

    maxim = max(data.compressed())
    minim = min(data.compressed())

    print len(data.compressed())

    # Gaussian Reference
    print "Fit Normal distribution curve to the data ... "
    mu, std = norm.fit(data.compressed())
    # sumv=np.sum(SSH)
    # print sumv
    print "Building histogram information ... "
    histo, bins = np.histogram(data.compressed(), bins=20)
    return (histo, bins, mu, std, a, v, s, k, maxim, minim)

def GetData(filePath, fileName):
    f = str(filePath + fileName)
    print 
    print " Reading in ", f
    print 
    data = np.genfromtxt(f)
    return data

def HistBin(x, y, xbins, ybins):
    # x and  y are arrays
    binPair = zip(x, y)

    if (ybins == None):
        ybins = xbins
    xdata, ydata = zip(*binPair)
    xmin = float(np.min(x))
    xmax = float(np.max(x))
    ymin = float(np.min(y))
    ymax = float(np.max(y))

    xwidth = xmax - xmin
    ywidth = ymax - ymin

    # This generates the bins
    def xbin(xval):
        return np.floor(xbins * (xval - xmin)/xwidth) if xmin <= xval < xmax else xbins - 1 if xval == xmax else None

    def ybin(yval):
        return np.floor(ybins * (yval - ymin)/ywidth) if ymin <= yval < ymax else ybins - 1 if yval == ymax else None

    hist = np.zeros((xbins, ybins))

    for x, y in binPair:
        i_x, i_y = xbin(x), ybin(ymax - y)
        if i_x is not None and i_y is not None:
            hist[i_x, i_y] += 1

    extent = (xmin, xmax, ymin, ymax)
    return (hist, extent)

def zero_to_nan(values):
    return [float('nan') if x==0 else x for x in values]

#-------------------------------------- PIGEON HOLE PROCESS --------------------------------------#
# This class is to help with the pigeon hole process. The member functions are:
# createExtent(stdDev, Lambda, N1) -> stdDev is defined previously and used to find the bin sizes
#                                       for the rest of the N's used. This will depend on how many
#                                       principal components we'll be using. Lambda is an array of
#                                       eigenvalues, which are defined as the variance for the 
#                                       corresponding principal component. avg is an array containing
#                                       the averages of the data. N1 is the amount of bins,
#                                       which is used to find the bin size.
#
# createSpace(extent, N1, N2) -> This function creates the bins given the extent, N1, and N2.
#                                   It returns an arbitrary amount of 'bin' arrays.
#----------------- Binning & Input of Characteristics -----------------#
class PigeonHole(object):
    def createExtent(self, NstdDev, sigma2, avg, Nbin):
        # N * delta T = 2 * 3 * sqrt(L1) -> 2 * standard_deviation_choice * sqrt(eigenvalue1)
        Ndim = sigma2.size
        arrayN = np.zeros(Ndim)
        arrayMin = np.zeros(Ndim)
        arrayMax = np.zeros(Ndim)
        BinSize = 2 * NstdDev * np.sqrt(sigma2[0]) / (Nbin)   # Get the BinSize

        for i in range(Ndim):
            arrayN[i] = np.ceil((1.0/BinSize) * (2.0 * NstdDev * np.sqrt(sigma2[i])))

        for i in range(Ndim):
            arrayMin[i] = avg[i] - (NstdDev * np.sqrt(sigma2[i]))
            arrayMax[i] = avg[i] + (NstdDev * np.sqrt(sigma2[i]))

        extent_and_sizes = [arrayMin, arrayMax, arrayN]
        return extent_and_sizes

    def createSpace(self, extent):
        # Unpack the extent list
        minArray = extent[0]
        maxArray = extent[1]
        nArray = extent[2] # How many many bins
        minN = minArray.size
        maxN = maxArray.size
        NN = nArray.size
        # Build the bins
        bins = [[] for i in range(NN)]
        for i in range(NN):
            bins[i].append( np.linspace(minArray[i], maxArray[i], nArray[i]+1) )
        return bins

    def getIndex(self, X, B):
        container = []
        Ndims = len(B)
        # Build set of iterators
        I = [[] for i in range(0, Ndims)]
        # Unpack bins
        for i in range(0, Ndims):
            container.append( np.array(B[i]) )

        for i in range(0, Ndims):
            notFound = True
            c = container[i]
            x = X[i]
            b = c[0]
            min_I = 0
            max_I = len(b)
            I[i] = 0

            while notFound == True:
                if x > b[I[i]]:
                    min_I = I[i]
                    I[i] = np.ceil( (max_I + min_I) / 2.0 )
                    if (np.abs(max_I - min_I) == 1.0) or (np.abs(max_I - min_I) == 0.0):
                        notFound = False
                elif x < b[I[i]-1]:
                    max_I = I[i]
                    I[i] = np.floor( (max_I + min_I) / 2.0 )
                    if np.abs(max_I - min_I) == 1.0 or (np.abs(max_I - min_I) == 0.0):
                        notFound = False
                else:
                    notFound = False
        return I




