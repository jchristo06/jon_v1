#!/Users/jchristo/anaconda/bin/python
#
#		build_X_matrix.py
#	Author: Jonathan Christophersen
#	Data: 04 June 2015
#
#	This python script is responsible for creating a database of calculated variables from the intersection data 
#	of AMSU-B radiometer and TRMM radar. The data was processed and put into files by process_Y.cpp. 
#	This script takes the data within those files and puts it together into a matrix.
##
#

# import modules
import matplotlib.pyplot as plt
import numpy as np
import os
import glob

fSize = 80 # How long the array should be - number of rows

# Build strings
# Input
ending = ".ascii"
year1 = "2009"
year2 = "2010"
filePath = "../../../files/"
filePath_python = "/Users/jonathanchristophersen/Research/AH5017/jon_V1/code/process_data/python/"
# filePath_python = "/Users/jchristo/AH5017/jon_V1/code/process_data/python"
rainFilePath = str(filePath + "X/rain/")
clearFilePath = str(filePath + "X/clear/")

# Output
outputPath = str(filePath + "data/X_mat/")
outputName_rain = "X_rain_mat.txt"
outputName_clear = "X_clear_mat.txt"
matFileName_rain = str(outputPath + outputName_rain)
matFileName_clear = str(outputPath + outputName_clear)

# open output file 
# Build directory if non-existant
# ../../../files/X_mat/
dir = os.path.dirname(matFileName_rain)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_rain, 'w+')

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

dir = os.path.dirname(matFileName_clear)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_clear, 'w+')

#-------------------------------- Work on matrix for the 2009 data --------------------------------#
# Change directory to the 2009 rain file folder
# and build the matrix for rainy cases
os.chdir(rainFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2009 rainy cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		rainMat2009 = f

	if i > 0:
		rainMat2009 = np.concatenate((rainMat2009, f))

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

# Change directory to the 2009 clear file folder
# and build the matrix for rainy cases
os.chdir(clearFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2009 clear cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		clearMat2009 = f

	if i > 0:
		clearMat2009 = np.concatenate((clearMat2009, f))


# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)
#-------------------------------- Work on matrix for the 2010 data --------------------------------#
# Change directory to the 2010 file folder
os.chdir(rainFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2010 rainy cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		rainMat2010 = f

	if i > 0:
		rainMat2010 = np.concatenate((rainMat2010, f))

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

# Change directory to the 2010 file folder
os.chdir(clearFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2010 clear cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		clearMat2010 = f

	if i > 0:
		clearMat2010 = np.concatenate((clearMat2010, f))


#-------------------------------- Gluing the pieces --------------------------------#
print "Putting 2009 and 2010 matrices together ......... ", "\n"
rainMat = np.concatenate((rainMat2009, rainMat2010))
rainMat = rainMat.T

clearMat = np.concatenate((clearMat2009, clearMat2010))
clearMat = clearMat.T

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

print "The shape of the matrices: ", "\n"
print "The rain matrix has shape ", rainMat.shape, "\n"
print "The clear matrix has shape ", clearMat.shape, "\n"

print "Outputting the matrices to ", outputPath, "\n"
np.savetxt(matFileName_rain, rainMat)
np.savetxt(matFileName_clear, clearMat)
