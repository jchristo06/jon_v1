
#include "global.h"

// Main data path - useful for input and output purposes
std::string data = "../../../files/data/";

/*------------------------------------------- For Input -------------------------------------------*/
// Path to x and y matrix files
std::string x_var = data + "X_mat/";
std::string y_var = data + "Y_mat/";
std::string h_var = data + "H_mat/";
std::string x_land_var = data + "X_land_mat/";
std::string y_land_var = data + "Y_land_mat/";
std::string test_path = data + "test/";
std::string total_tb_var = data + "total_tb_mat/";
std::string cwm_var = data + "CWM_mat/";

// FILE NAMES:
// Rain
std::string x_rain = "X_rain_mat.txt";
std::string y_rain = "Y_rain_mat.txt";
std::string h_rain = "H_rain_mat.txt";

// Clear
std::string x_clear = "X_clear_mat.txt";
std::string y_clear = "Y_clear_mat.txt";

// Ice
std::string x_ice = "X_ice_mat.txt";
std::string y_ice = "Y_ice_mat.txt";
std::string h_ice = "H_ice_mat.txt";

// Mix
std::string x_mix = "X_mix_mat.txt";
std::string y_mix = "Y_mix_mat.txt";
std::string h_mix = "H_mix_mat.txt";

// Total Brightness temps
std::string tb_rain = "total_tb_rain_mat.txt";
std::string tb_clear = "total_tb_clear_mat.txt";

// Land rain
std::string xL_rain = "X_land_rain_mat.txt";
std::string yL_rain = "Y_land_rain_mat.txt";

// Land clear
std::string xL_clear = "X_land_clear_mat.txt";
std::string yL_clear = "Y_land_clear_mat.txt";

// Land ice
std::string xL_ice = "X_land_ice_mat.txt";
std::string yL_ice = "Y_land_ice_mat.txt";

// Land mix
std::string xL_mix = "X_land_mix_mat.txt";
std::string yL_mix = "Y_land_mix_mat.txt";

// CWM
std::string cwm_x = "CWM_radar_mat.txt";
std::string cwm_y = "CWM_Tb_mat.txt";

// Test
std::string test_fn = "test_matrix.txt";

// GLUE STRINGS TOGETHER:
// Rain
std::string xr = x_var + x_rain;
std::string yr = y_var + y_rain;
std::string hr = h_var + h_rain;

// Clear
std::string xc = x_var + x_clear;
std::string yc = y_var + y_clear;

// Ice
std::string xi = x_var + x_ice;
std::string yi = y_var + y_ice;
std::string hi = h_var + h_ice;

// Mix
std::string xm = x_var + x_mix;
std::string ym = y_var + y_mix;
std::string hm = h_var + h_mix;

// Total brightness temps
std::string tbr = total_tb_var + tb_rain;
std::string tbc = total_tb_var + tb_clear;

// Land rain
std::string xLr = x_land_var + xL_rain;
std::string yLr = y_land_var + yL_rain;

// Land ice
std::string xLi = x_land_var + xL_ice;
std::string yLi = y_land_var + yL_ice;

// Land mix
std::string xLm = x_land_var + xL_mix;
std::string yLm = y_land_var + yL_mix;

// Land clear
std::string xLc = x_land_var + xL_clear;
std::string yLc = y_land_var + yL_clear;

// CWM
std::string xCwm = cwm_var + cwm_x;
std::string yCwm = cwm_var + cwm_y;

// Test
std::string test = test_path + test_fn;

/*------------------------------------------- For Output -------------------------------------------*/
std::string c_output = data + "calc_output/";

// Rain
std::string cxx_r = c_output + "Cxx_rain.txt";
std::string cyy_r = c_output + "Cyy_rain.txt";
std::string cxy_r = c_output + "Cxy_rain.txt";
std::string chh_r = c_output + "Chh_rain.txt";
std::string txx_r = c_output + "Txx_rain.txt";
std::string txy_r = c_output + "Txy_rain.txt";
std::string NormX_r = c_output + "NormX_rain.txt";
std::string NormY_r = c_output + "NormY_rain.txt";
std::string NormH_r = c_output + "NormH_rain.txt";


// Clear
std::string cxx_c = c_output + "Cxx_clear.txt";
std::string cyy_c = c_output + "Cyy_clear.txt";
std::string cxy_c = c_output + "Cxy_clear.txt";
std::string txx_c = c_output + "Txx_clear.txt";
std::string txy_c = c_output + "Txy_clear.txt";
std::string NormX_c = c_output + "NormX_clear.txt";
std::string NormY_c = c_output + "NormY_clear.txt";

// Ice
std::string cxx_i = c_output + "Cxx_ice.txt";
std::string cyy_i = c_output + "Cyy_ice.txt";
std::string cxy_i = c_output + "Cxy_ice.txt";
std::string chh_i = c_output + "Chh_ice.txt";
std::string txx_i = c_output + "Txx_ice.txt";
std::string txy_i = c_output + "Txy_ice.txt";
std::string NormX_i = c_output + "NormX_ice.txt";
std::string NormY_i = c_output + "NormY_ice.txt";
std::string NormH_i = c_output + "NormH_ice.txt";

// Mix
std::string cxx_m = c_output + "Cxx_mix.txt";
std::string cyy_m = c_output + "Cyy_mix.txt";
std::string cxy_m = c_output + "Cxy_mix.txt";
std::string chh_m = c_output + "Chh_mix.txt";
std::string txx_m = c_output + "Txx_mix.txt";
std::string txy_m = c_output + "Txy_mix.txt";
std::string NormX_m = c_output + "NormX_mix.txt";
std::string NormY_m = c_output + "NormY_mix.txt";
std::string NormH_m = c_output + "NormH_mix.txt";

// Rain over the land
std::string cxx_l_r = c_output + "Cxx_land_rain.txt";
std::string cyy_l_r = c_output + "Cyy_land_rain.txt";
std::string cxy_l_r = c_output + "Cxy_land_rain.txt";
std::string chh_l_r = c_output + "Chh_land_rain.txt";
std::string txx_l_r = c_output + "Txx_land_rain.txt";
std::string txy_l_r = c_output + "Txy_land_rain.txt";
std::string NormX_l_r = c_output + "NormX_land_rain.txt";
std::string NormY_l_r = c_output + "NormY_land_rain.txt";
std::string NormH_l_r = c_output + "NormH_land_rain.txt";

// Clear over the land
std::string cxx_l_c = c_output + "Cxx_land_clear.txt";
std::string cyy_l_c = c_output + "Cyy_land_clear.txt";
std::string cxy_l_c = c_output + "Cxy_land_clear.txt";
std::string txx_l_c = c_output + "Txx_land_clear.txt";
std::string txy_l_c = c_output + "Txy_land_clear.txt";
std::string NormX_l_c = c_output + "NormX_land_clear.txt";
std::string NormY_l_c = c_output + "NormY_land_clear.txt";

// Ice over the land
std::string cxx_l_i = c_output + "Cxx_land_ice.txt";
std::string cyy_l_i = c_output + "Cyy_land_ice.txt";
std::string cxy_l_i = c_output + "Cxy_land_ice.txt";
std::string chh_l_i = c_output + "Chh_land_ice.txt";
std::string txx_l_i = c_output + "Txx_land_ice.txt";
std::string txy_l_i = c_output + "Txy_land_ice.txt";
std::string NormX_l_i = c_output + "NormX_land_ice.txt";
std::string NormY_l_i = c_output + "NormY_land_ice.txt";
std::string NormH_l_i = c_output + "NormH_land_ice.txt";

// Mix over the land
std::string cxx_l_m = c_output + "Cxx_land_mix.txt";
std::string cyy_l_m = c_output + "Cyy_land_mix.txt";
std::string cxy_l_m = c_output + "Cxy_land_mix.txt";
std::string chh_l_m = c_output + "Chh_land_mix.txt";
std::string txx_l_m = c_output + "Txx_land_mix.txt";
std::string txy_l_m = c_output + "Txy_land_mix.txt";
std::string NormX_l_m = c_output + "NormX_land_mix.txt";
std::string NormY_l_m = c_output + "NormY_land_mix.txt";
std::string NormH_l_m = c_output + "NormH_land_mix.txt";


std::string NormTb_r = c_output + "NormTb_rainy.txt";
std::string Tb_r = c_output + "Total_Tb_rainy.txt";

// CWM
std::string cxx_cwm = c_output + "Cxx_cwm.txt";
std::string cyy_cwm = c_output + "Cyy_cwm.txt";
std::string cxy_cwm = c_output + "Cxy_cwm.txt";
std::string NormX_cwm = c_output + "NormX_cwm.txt";
std::string NormY_cwm = c_output + "NormY_cwm.txt";



