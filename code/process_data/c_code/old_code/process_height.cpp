/*
  Read PR-AMSU database files with the full 80-bin database format

  NOAA-15/16/17 have AMSUA and AMSUB (89,150,183/1,183/3,183/7)
  NOAA-18/19 and MetOp-A have AMSUA and MHS (89,157,183/1,183/3,190)

  Datasets used to create the matchups have these conventions:
    PR filename syntax: 2A25.20090424.65179.7.HDF
    AMSUB/MHS filename syntax: SWATH_N19_MHS_S09280_052428_E09280_071713.hdf
  so the matchup filenames are derived from these for now.  Can be changed
  to whatever you want.

  -------------------------------------------------------------------------------
  AMSUB/MHS resolution filename syntax:
    2A25.20080412.59305.7.MOA_MHS_S08103_215243_E08103_233534.hdf.bin
  where the first part of the filename is taken from the V7 2A25 file, and the second part is from
  the name of the AMSUB/MHS filename from NESDIS/STAR, where MOA=MetOp-A, and N15, N16, N17, N18, N19 indicate the satellite platform.

  For files at the AMSUB/MHS resolution:  PR profiles are matched to the AMSUB/MHS pixels, taking
  into account the changing AMSUB/MHS FOV shape and orientation.  Then the closest AMSUA pixel is found.
  The time and latitude/longitude in the record are the AMSUB coordinates.
  The sfcrain in the record is the average of the 2A25 surfaceRain for all PR beams that fall inside the AMSUB/MHS pixel.
  The 80-bin mass content profile is computed by interpolating the precipWaterParmA and B values onto the 80-bin correctedZ profile, using
  the ParmNode levels to separate the profile into ice (between levels A and B), mixture ( between levels B and D) and rain ( between levels D and E).
  (using the same method that Sarah Ringerud used).
  This is done for all PR beams that fall into the AMSUB/MHS FOV.  Then all of these 80-bin
  rwc, mwc and iwc profiles that fall inside the AMSUB/MHS FOV are binned to the 28 layers specified in the CSU profile format (500-m spacing up to
  10-km, then 1-km to 18-km).

  A: Echo top (1)
  B: Above Freezing Level (2)
  C: Freezing Level (3)
  D: Below Freezing Level (4)
  E: Near Surface (5) 

  tb1 indices:  0= AMSUA sat_zenith in degrees, 1= AMSUB/MHS-AMSUA time difference in seconds, 2= AMSUB/MHS-AMSUA pixel offset in km
                3-9 are AMSUA channels 1-7.
  tb2 indices:  0= AMSUB sat_zenith in degrees, 1= AMSUB/MHS-PR time difference in seconds, 2= AMSUB/MHS-PR pixel offset in km
                3-7 are AMSUB channels 1-5.

  -------------------------------------------------------------------------------
  AMSUA resolution filename syntax:
    2A25.20080412.59305.7.MOA_AMSUA_S08103_215238_E08103_233526.hdf.bin
  which is the same as above, but the second part is the name of the
  AMSUA filename from NESDIS/STAR, where MOA=MetOp-A, and N15, N16, N17, N18, N19 indicate the satellite platform.

  For files at the AMSUA resolutiion:  Same procedure as above, but the PR profiles are matched to the AMSUA pixels, taking
  into account the changing AMSUA FOV shape and orientation.  Then the closest AMSUB/MHS pixel is found
  and a surrounding 3x3 average is made to get the AMSUB/MHS TB.
  The time and latitude/longitude in the record are the AMSUA coordinates.
  sfcrain, rwc, mwc, and iwc are computed same as above.

  tb1 indices:  0= AMSUA sat_zenith in degrees, 1= AMSUA-PR time difference in seconds, 2= AMSUA-PR pixel offset in km
                3-9 are AMSUA channels 1-7.
  tb2 indices:  0= AMSUB sat_zenith in degrees, 1= AMSUA-AMSUB/MHS time difference in seconds, 2= AMSUA-AMSUB/MHS pixel offset in km
                3-7 are AMSUB channels 1-5.

  -------------------------------------------------------------------------------

  cwc and lh profiles are all set to -99.99


  28 Dec 2011  J. Turk   jturk@jpl.nasa.gov

*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <strings.h>
#include <time.h>

using namespace std;

int main(int argc, char *argv[])
{

  FILE *fdb;

 typedef struct {
    float lat,lon;
    int yyyy,mm,dd,hh,mn,ss;
    int sfc_ab;
    int ray_index_pr, scan_index_ab, scan_index_aa;
    int npr, nrain, nstrat, nconv;
    float satzen_aa, dist_ab_aa, time_ab_aa;
    float satzen_ab, dist_ab_pr, time_ab_pr;
    float sfcrain, sfcrain_nonzero, sfcrain_max;
    float binrain, binrain_nonzero, binrain_max;
    float sigma0, sigma0_min, sigma0_max, freezh;
    float pia, pia_max, loczen_pr;
    float rr_ab;
    float rwc_total, mwc_total, iwc_total;
    float tb_aa[15],tb_ab[5];
    float rwc[80];
    float mwc[80];
    float iwc[80];
    float rwc_max[80];
    float mwc_max[80];
    float iwc_max[80];
    float z[80];
    float zmax[80];
    float rr[80];
    float rr_nonzero[80];
    float rr_max[80];
  } matchup_record;

  int counter = 0;
  int i, j, bytes, nrec=0;
  float ht;
  matchup_record prof;

  /* Create lat/lon Boxes */
  float lat_lower = -22.0;
  float lat_upper = 22.0;
  float lon_left = -30;
  float lon_right = 20;

  /* Create time intervals */
  int month1 = 1;
  int month2 = 12;

  float maxht = 0.0;

  // Initialize two-dimensional array. This is for output in columnar form.
  int ndata = 100;
  int nstate = 80;
  float X[1000][80]; // State variables - iwc, rwc, mwc. The size of which is ndata x nstate
  float H[1000]; // Highest point of State variables - iwc, rwc, mwc. The size of which is ndata x nstate
  float dummy_ht[80];

  int year[ndata];
  int month[ndata];
  int day[ndata];
  int hour[ndata];
  int min[ndata];
  int sec[ndata];

  if (argc != 2 ) exit(-1);
  bytes = sizeof(matchup_record);
  /*printf("record size=%d\n", bytes);*/

  if ((fdb = fopen(argv[1],"r")) == 0) {
    printf("Unable to open %s\n", argv[1]);
    exit(-1);
  }

while ( fread(&prof,bytes,1,fdb) == 1 ) 
{
  if ( prof.lat >= lat_lower && prof.lat <= lat_upper && prof.lon >= lon_left && prof.lon <= lon_right) // Create lat/lon box
  {
    if (prof.mm >= month1 && prof.mm <= month2) // Seasons
    {
      if ( fabs(prof.satzen_ab) >= 0.0 && fabs(prof.satzen_ab) <= 20.0 ) // Zenith angles intervals to use: 0-20, 20-40, 40+
      {   
        if ( prof.rwc_total > 0 ) // Detection of "rainy cases"
        {
            /*printf("%4d%02d%02d %02d%02d%02d\n ",prof.yyyy, prof.mm, prof.dd, prof.hh, prof.mn, prof.ss);
            printf("%8.3f %8.3f %2d\n ",prof.lat, prof.lon, prof.sfc_ab);
            printf("%d\n", prof.nrain);
            printf("%4d%02d%02d %02d%02d%02d ",prof.yyyy, prof.mm, prof.dd, prof.hh, prof.mn, prof.ss);
            printf("%8.3f %8.3f %2d ",prof.lat, prof.lon, prof.sfc_ab);
            printf("%6.2f %4d ",prof.dist_ab_pr, (int) prof.time_ab_pr);
            printf("%4d %4d %4d ",prof.scan_index_ab, prof.scan_index_aa, prof.ray_index_pr);
            printf("%6.2f %6.2f %6.2f ",prof.satzen_ab, prof.satzen_aa, prof.loczen_pr);
            printf("%4d %4d %4d %4d ",prof.npr, prof.nrain, prof.nstrat, prof.nconv);
            printf("%6.2f %6.2f %6.2f ", prof.sfcrain, prof.sfcrain_nonzero, prof.sfcrain_max);
            printf("%6.2f %6.2f %6.2f ", prof.binrain, prof.binrain_nonzero, prof.binrain_max);
            printf("%6.2f %6.2f %6.2f %6.2f %6.2f ",prof.pia, prof.pia_max, prof.sigma0, prof.sigma0_min, prof.sigma0_max);
            printf("%5d ",(int) prof.freezh);
            printf("%6.2f %6.2f %6.2f ",prof.rwc_total, prof.mwc_total, prof.iwc_total);*/

          // Capture the current date and time
          // year[i] = prof.yyyy;
          // month[i] = prof.mm;
          // day[i] = prof.dd;
          // hour[i] = prof.hh;
          // min[i] = prof.mn;
          // sec[i] = prof.ss;
          // printf("%6.2f\n ", prof.iwc_total);
          for (j=0; j < 80; j++)
          {
            ht = 19.750 - (j*0.250);
            dummy_ht[j] = 19.750 - (j*0.250);
            
            if (prof.rwc[j] > 0 && maxht <= dummy_ht[j])
            {
            	maxht = dummy_ht[j];
            	printf("%f ", ht);
            	printf("\n");
			    printf("\n");
            }

            // X[i][j] = prof.iwc[j];
            
            // if (prof.iwc[j] > 0 && maxht > dummy_ht[j+1])
            // {
            	// printf("%f ", ht);
              // H[i] = ht;
            	// printf("%s\n", "dfjkasljkl");
            	// printf("\n");
			    // printf("\n");
            // }
          }
          maxht = 0.0;
          i += 1;
          counter += 1;
        }
      }
    }
  }
}

  // for (i = 0; i < counter; i++)
  // {
    // printf("%4d%02d%02d %02d%02d%02d\n ", year[i], month[i], day[i], hour[i], min[i], sec[i]);
    // for (j = 0; j < 80; j++)
    // {
    //   printf("%f ", X[i][j]);
    // }
    // printf("%f ", H[i]);
    // printf("\n");
    // printf("\n");
  // }

  fclose(fdb);

  exit(0);

}

