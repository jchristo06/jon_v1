#!/bin/bash
#
# This script prompts the user to choose which matrix to build and then runs the python scripts to 
# create the matrices.
#
#

echo "______________________________________________________________________________________________________"
echo "Which variable matrix would you like to build?"
echo "1) Y: observational space"
echo "2) X: model state space"
echo "3) Y_land: observational space over land only"
echo "4) X_land: model state space over land"
echo "5) Height: height of the highest level with a particular threshold of precip"
echo "6) all values: (1-5)"
echo "______________________________________________________________________________________________________"

read VAR

if [ $VAR -eq 1 ]; then
	echo " Building Y matrix "
	python build_Y_matrix.py
elif [ $VAR -eq 2 ]; then
	echo " Building X matrix "
	python build_X_matrix.py
elif [ $VAR -eq 3 ]; then
	echo " Building Y land matrix "
	python build_Y_land_matrix.py
elif [ $VAR -eq 4 ]; then
	echo " Building X land matrix "
	python build_X_land_matrix.py
elif [ $VAR -eq 5 ]; then
	echo " Building height matrix "
	python build_height_matrix.py
elif [ $VAR -eq 6 ]; then
	echo " Building Y matrix "
	python build_Y_matrix.py
	echo " Building X matrix "
	python build_X_matrix.py
	echo " Building Y_land matrix "
	python build_Y_land_matrix.py
	echo " Building X_land matrix "
	python build_X_land_matrix.py
	echo " Building height matrix "
	python build_height_matrix.py
fi
