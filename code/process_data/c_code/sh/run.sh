#!/bin/bash
#	Uses read_PR_MHS_full.c to read the files containing data that consists of the intersections of radiometer and radar for the
# 	years of 2009 and 2010.
#	This script will be used to count how many times the intersections of these two instruments occur given constraints of lat/lon,
# 	time (seasonal: DJF, MAM, JJA, SON; off-seasonal: FMA, MJJ, ASO, NDJ), as well as other variables (listed below). 
#	We want to count how many "rainy" cases occur based on 
#	the fields from lines 132 - 147 in read_PR_MHS_full.c. We mainly want variables:
#		



#############################################################################################
#																							#
#																							#
#									LIST of PARAMATERS										#
#																							#
#				List of parameters that are available in the *.cpp files 					#
#																							#
#############################################################################################

#		1. yyyy: year
#		2. mm: month
#		3. dd: day
#		4. hh: hour
#		5. mn: minute
# 		6. ss: seconds
#		7. lat: latitude
# 		8. lon: longitude
#		9. sfc_ab: surface reading from AMSU-B
#		10. dist_ab_pr: distance from AMSU-B radiometer to radar
#		11. time_ab_pr
#		12. scan_index_ab
#		13. scan_index_aa
#		14. ray_index_pr
# 		15. satzen_ab: Zenith angle
# 		16. satzen_aa
#		17. loczen_pr
#		18. npr: Number of radar beams that were averaged in a given swath
#		19. nrain: how many of those beams had rain
#		20. nstrat: how many had stratification
#		21. nconv: how many were convection
#		22. sfcrain: average of the 2A25 surfaceRain for all PR beams that fall inside the AMSUB/MHS pixel. mm/s
#		23. sfcrain_nonzero
#		24. sfcrain_max
#		25. binrain
#		26. binrain_nonzero
#		27. binrain_max
#		28. pia: path integrated attenuation
#		29. pia_max
# 		30. sigma0
#		31. sigma0_min
#		32. sigma0_max
#	 	33. freezh
#		34. rwc_total: total liquid content (kg/m^2)
#		35. mwc_total: total mixed (liquid/ice) content (kg/m^2)
#	 	36. iwc_total: total ice content - this one is important!!! (kg/m^2)
#		37. tb_aa: brightness temperature of AMSU-A
#		38. tb_ab: brightness temperature of AMSU-B
#		39. z: Average radar reflectivy per level
#		40. zmax: Max radar reflectivity per level
#		41. rr: Rain rate in mm/s
#		42. rr_nonzero
#		43. rr_max
#		44. rwc: Average rain water content per level
#		45. mwc: Average mixed ice/water content per level
#		46. iwc: Average ice content per level


# File list of parameters
FILE=filelist

# Make sure filelist doesn't exist in the directory
rm $FILE


#############################################################################################
#																							#
#																							#
#									PARAMETER SETUP											#
#																							#
#	 			Set of parameters to be used in the corresponding *.cpp files 				#
#																							#
#																							#
#############################################################################################
lat_lower=-22.0
lat_upper=22.0
lon_left=-180
lon_right=180
month1=1
month2=12
geoparam=ice	# options are: rain, ice, mix, clear

# 0 for clear, 1 for rainy
if [[ $geoparam == clear ]]; then
	rainy=0
else
	rainy=1
fi

# Make filelist
echo $lat_lower >> $FILE
echo $lat_upper >> $FILE
echo $lon_left >> $FILE
echo $lon_right >> $FILE
echo $month1 >> $FILE
echo $month2 >> $FILE
echo $geoparam >> $FILE
echo $rainy >> $FILE

cp $FILE ../cpp/$FILE

echo "______________________________________________________________________________________________________"
echo "Which variable would you like to run the processing data for?"
echo "1) Y: observational space"
echo "2) X: model state space"
echo "3) Y_land: observational space over land only"
echo "4) X_land: model state space over land"
echo "5) Height: height of the highest level with a particular threshold of precip"
echo "6) all values: (1-5)"
echo "7) RR: Rain Rates"
echo "______________________________________________________________________________________________________"

read VAR

echo " PROCESSING $geoparam DATA "
if [ $VAR -eq 1 ]; then
	sh process_Y.sh
elif [ $VAR -eq 2 ]; then
	sh process_X.sh
elif [ $VAR -eq 3 ]; then
	sh process_Y_land.sh
elif [ $VAR -eq 4 ]; then
	sh process_X_land.sh
elif [ $VAR -eq 5 ]; then
	sh process_height.sh
elif [ $VAR -eq 6 ]; then
	sh process_Y.sh
	sh process_X.sh
	sh process_Y_land.sh
	sh process_X_land.sh
	if [[ $geoparam != clear ]]; then
		sh process_height.sh
	fi
elif [ $VAR -eq 7 ]; then
	sh process_RR.sh
fi

rm $FILE
echo " Cleaning ../cpp/ folder "
rm ../cpp/$FILE
rm ../cpp/*.exe





