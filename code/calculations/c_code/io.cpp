#include "io.h"

using namespace std;

/*																Input Class																		*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
std::vector< std::vector<double> > Input::read2DVector( std::string fp )
{
	ifstream fin;
	int row, col; // For getting dimension size of the matrix
	std::string line;
	std::vector< std::vector<double> > v;

	cout << "Opening files .... " << "\n";
	cout << fp << "\n";
	fin.open(fp.c_str());
	if (!fin) exit(-1);

	cout << "Reading matrix data .... " << "\n";

	while (std::getline(fin, line))
	{
		v.push_back(std::vector<double>());

		std::stringstream split(line);
		float value;

		while (split >> value)
			v.back().push_back(value);
	}

	cout << "The matrix shape is: " << "\n";
	cout << "(" << v.size() << ", " << v[0].size() << ")";
	cout << "\n";
	cout << "\n";

	return v;
}


/*																Output Class																	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


void Output::write2DVector( std::string fp, std::vector< std::vector<double> > v )
{
	ofstream fout(fp.c_str());

	cout << "Writing matrix information to " << fp << "\n";

	for (int i = 0; i < v.size(); i++)
	{
		v[i].resize(v[i].size());
		for (int j = 0; j < v[i].size(); j++)
		{
			double a = v[i][j];
			fout << a << " ";
		}
		fout << "\n";
	}
}

void Output::write1DVector( std::string fp, std::vector<double> v )
{
	ofstream fout(fp.c_str());

	cout << "Writing matrix information to " << fp << "\n";

	for (int i = 0; i < v.size(); i++)
	{
		fout << v[i] << "\n";
	}
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


































