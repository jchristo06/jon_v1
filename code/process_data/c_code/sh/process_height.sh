#!/bin/bash
#	Uses read_PR_MHS_full.c to read the files containing data that consists of the intersections of radiometer and radar for the
# 	years of 2009 and 2010.
#	This script will be used to count how many times the intersections of these two instruments occur given constraints of lat/lon,
# 	time (seasonal: DJF, MAM, JJA, SON; off-seasonal: FMA, MJJ, ASO, NDJ), as well as other variables (listed below). 
#	We want to count how many "rainy" cases occur based on 
#	the fields from lines 132 - 147 in read_PR_MHS_full.c. We mainly want variables:
#		
#		1. yyyy: year
#		2. mm: month
#		3. dd: day
#		4. hh: hour
#		5. mn: minute
# 		6. ss: seconds
#		7. lat: latitude
# 		8. lon: longitude
#		9. sfc_ab: surface reading from AMSU-B
#		10. dist_ab_pr: distance from AMSU-B radiometer to radar
#		11. time_ab_pr
#		12. scan_index_ab
#		13. scan_index_aa
#		14. ray_index_pr
# 		15. satzen_ab: Zenith angle
# 		16. satzen_aa
#		17. loczen_pr
#		18. npr: Number of radar beams that were averaged in a given swath
#		19. nrain: how many of those beams had rain
#		20. nstrat: how many had stratification
#		21. nconv: how many were convection
#		22. sfcrain: average of the 2A25 surfaceRain for all PR beams that fall inside the AMSUB/MHS pixel. mm/s
#		23. sfcrain_nonzero
#		24. sfcrain_max
#		25. binrain
#		26. binrain_nonzero
#		27. binrain_max
#		28. pia: path integrated attenuation
#		29. pia_max
# 		30. sigma0
#		31. sigma0_min
#		32. sigma0_max
#	 	33. freezh
#		34. rwc_total: total liquid content (kg/m^2)
#		35. mwc_total: total mixed (liquid/ice) content (kg/m^2)
#	 	36. iwc_total: total ice content - this one is important!!! (kg/m^2)
#		37. tb_aa: brightness temperature of AMSU-A
#		38. tb_ab: brightness temperature of AMSU-B
#		39. z: Average radar reflectivy per level
#		40. zmax: Max radar reflectivity per level
#		41. rr: Rain rate in mm/s
#		42. rr_nonzero
#		43. rr_max
#		44. rwc: Average rain water content per level
#		45. mwc: Average mixed ice/water content per level
#		46. iwc: Average ice content per level

# File list of parameters
FILE=filelist

# Read last line of filelist
# to get rain information
RAIN=`tail -1 $FILE`
TYPE=`tail -2 $FILE | head -1`

COUNT=001
ENDCOUNT=989

YEAR=2009
VAR=height
DATAPATH=../../../../../N19/$YEAR

if [[ $RAIN -eq 1 ]]; then
	OUTPUTPATH=../../../../files/$VAR/$TYPE
else
	OUTPUTPATH=../../../../files/$VAR/clear
fi

# Compile read_PR_MHS_full.c
echo "Compiling Heights"
g++ ../cpp/process_$VAR.cpp -o ../cpp/process_$VAR.exe

# cd $DATAPATH
while [ $COUNT -le $ENDCOUNT ]
do
	IFILE=$DATAPATH/$COUNT
	OFILE=$OUTPUTPATH/$NAME_$COUNT.ascii

	../cpp/process_$VAR.exe $IFILE > $OFILE

	COUNT=`expr $COUNT + 1`	

	if [ $COUNT -lt 10 ]; then
		COUNT="00"$COUNT
	else
		if [ $COUNT -lt 100 -a $COUNT -ge 10 ]; then
			COUNT="0"$COUNT
		fi
	fi

	if [ $COUNT -eq 621 ]; then #End of first years data
		YEAR=2010
		DATAPATH=../../../../../N19/$YEAR
	fi

done

if [ $RAIN -eq 1 ]; then
	find ../../../../files/$VAR/$TYPE -size 0 -exec rm -f {} \;
else
	find ../../../../files/$VAR/clear -size 0 -exec rm -f {} \;
fi