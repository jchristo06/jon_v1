#!/Users/jchristo/anaconda/bin/python
#
#
#
#
#

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

# Principal component and eigenvalue data path
pca_path = "../../../files/data/pca/"

#------------------------------ DATA PROCESSING ------------------------------#
# Principal Components
pcScore_yy_rain = np.genfromtxt(str(pca_path + "pcScores_yy_rain.txt"))
pcScore_yy_clear = np.genfromtxt(str(pca_path + "pcScores_yy_clear.txt"))
pc1_r = pcScore_yy_rain[0, :]
pc2_r = pcScore_yy_rain[1, :]
pc1_c = pcScore_yy_clear[0, :]
pc2_c = pcScore_yy_clear[1, :]

# Eigen values to get the standard deviation
lambda_yy_clear_fn = "lambdayy_clear.txt"
lambda_yy_rain_fn = "lambdayy_rain.txt"
lambda_yy_clear = str(pca_path + lambda_yy_clear_fn)
lambda_yy_rain = str(pca_path + lambda_yy_rain_fn)
Lambdayy_clear = np.genfromtxt(lambda_yy_clear)
Lambdayy_rain = np.genfromtxt(lambda_yy_rain)
# Sort from lowest to highest
Lambdayy_clear = np.sort(Lambdayy_clear)
Lambdayy_rain = np.sort(Lambdayy_rain)
# First eigenvalue
L1_yc = np.sqrt(Lambdayy_clear[-1])
L1_yr = np.sqrt(Lambdayy_rain[-1])
# Second eigenvalue
L2_yc = np.sqrt(Lambdayy_clear[-2])
L2_yr = np.sqrt(Lambdayy_rain[-2])

# Spacing stays the same for the second Principal Component, but the number of bins will change.
# N * delta T = 2 * 3 * sqrt(L1) -> 2 * standard_deviation_choice * sqrt(eigenvalue1)
N1 = 100	# Number of bins for PC1
stdDev = 3		# we are interested in points 'stdDev' out from the mean, in both directions
BinSize = 2 * stdDev * L1_yr / N1	# +- the standard deviation means twice 'stdDev'
N2 = int( (1 / BinSize) * (2 * stdDev * L2_yr) )	# Number of bins based on PC 2

# Find the average of the PCs.
mu1_r = np.mean(pc1_r)
mu2_r = np.mean(pc2_r)
mu1_c = np.mean(pc1_c)
mu2_c = np.mean(pc2_c)

# For rainy conditions
min_pc1_r = mu1_r - (stdDev * L1_yr)
max_pc1_r = mu1_r + (stdDev * L1_yr)
min_pc2_r = mu2_r - (stdDev * L2_yr)
max_pc2_r = mu2_r + (stdDev * L2_yr)
# For clear-sky conditions
min_pc1_c = mu1_c - (stdDev * L1_yc)
max_pc1_c = mu1_c + (stdDev * L1_yc)
min_pc2_c = mu2_c - (stdDev * L2_yc)
max_pc2_c = mu2_c + (stdDev * L2_yc)

extent1_r = [min_pc1_r, max_pc1_r]
extent2_r = [min_pc2_r, max_pc2_r]
extent1_c = [min_pc1_c, max_pc1_c]
extent2_c = [min_pc2_c, max_pc2_c]

# Build the bins
binRange1_r = np.linspace(min_pc1_r, max_pc1_r, N1)
binRange2_r = np.linspace(min_pc2_r, max_pc2_r, N2)

counterVec = np.zeros(binRange1_r.size)
histoVec = np.zeros((binRange1_r.size, binRange2_r.size))

for i in range(pc1_r.size):
	pcElement1_r = pc1_r[i]

	for j in range(binRange1_r.size - 1):
		if ( pcElement1_r >= binRange1_r[j] ) & ( pcElement1_r < binRange1_r[j + 1] ):

			for k in range(binRange2_r.size - 1):
				if ( pcElement1_r >= binRange2_r[k] ) & ( pcElement1_r < binRange2_r[k + 1] ):
					histoVec[j, k] += 1
			# print binRange1_r[j], '   ', pcElement1_r, '   ', binRange1_r[j + 1]


# ----------------------------- SCATTER -----------------------------#
fig, ax = plt.subplots()
ax.pcolor(histoVec, cmap=plt.cm.Reds, edgecolors='k')
ax.set_xticks(binRange1_r)
ax.set_yticks(binRange2_r)
plt.savefig('./test.jpg')
plt.close()

# ----------------------------- HISTOGRAMS -----------------------------#
# w = 0.1

# 3D bar plot
# dx = BinSize
# dy = BinSize
# fig = plt.figure()
# ax1 = fig.add_subplot(111, projection='3d')


# plt.bar(binRange1_r, histoVec, width=w)
# plt.xlim(min(binRange1_r), max(binRange1_r))
# plt.title('Histogram for Principal Component 1 of Brightness Temperatures for Rainy Conditions', fontsize=10)
# plt.xlabel('Principal Components')
# plt.ylabel('Frequency')
# plt.savefig("./figs/histoPC1_r.pdf")
# plt.close()

# [H, binEdges] = np.histogram(pc1_r, bins=100)
# plt.bar(binEdges[:-1], H, width=w)
# plt.xlim(min(binEdges), max(binEdges))
# plt.title('Histogram for Principal Component 1 of Brightness Temperatures for Rainy Conditions', fontsize=10)
# plt.xlabel('Principal Components')
# plt.ylabel('Frequency')
# plt.savefig("./figs/histoPC1_r.pdf")
# plt.close()

# [H, binEdges] = np.histogram(pc2_r, bins=100)
# plt.bar(binEdges[:-1], H, width=w/4)
# plt.xlim(min(binEdges), max(binEdges))
# plt.title('Histogram for Principal Component 2 of Brightness Temperatures for Rainy Conditions', fontsize=10)
# plt.xlabel('Principal Components')
# plt.ylabel('Frequency')
# plt.savefig("./figs/histoPC2_r.pdf")
# plt.close()

# [H, binEdges] = np.histogram(pc1_c, bins=100)
# plt.bar(binEdges[:-1], H, width=w)
# plt.xlim(min(binEdges), max(binEdges))
# plt.title('Histogram for Principal Component 1 of Brightness Temperatures for Clear-Sky Conditions', fontsize=10)
# plt.xlabel('Principal Components')
# plt.ylabel('Frequency')
# plt.savefig("./figs/histoPC1_c.pdf")
# plt.close()

# [H, binEdges] = np.histogram(pc2_c, bins=100)
# plt.bar(binEdges[:-1], H, width=w)
# plt.xlim(min(binEdges), max(binEdges))
# plt.title('Histogram for Principal Component 2 of Brightness Temperatures for Clear-Sky Conditions', fontsize=10)
# plt.xlabel('Principal Components')
# plt.ylabel('Frequency')
# plt.savefig("./figs/histoPC2_c.pdf")
# plt.close()
