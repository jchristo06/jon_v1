#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "global.h"
#include "operations.h"
#include "stats.h"
#include "io.h"


/* This class handles the testing algorithms for error-checking most of the code found in operations.cpp. */
class Testing
{
public:
	void ShowMatrix( std::vector< std::vector<double> > v );
	std::vector<double> TestAvg( std::vector< std::vector<double> > v );
	std::vector<double> TestVar( std::vector< std::vector<double> > v, std::vector<double> avg );
	std::vector<double> TestStd( std::vector<double> variance );
	std::vector< std::vector<double> > TestNormalize( std::vector< std::vector<double> > v, std::vector<double> avg, std::vector<double> std );
	std::vector< std::vector<double> > TestMatrixTranspose( std::vector< std::vector<double> > v );
	std::vector< std::vector<double> > TestDotProduct( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 );
	void TestCovariance( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 );

	double TestAvg_All( std::vector< std::vector<double> > v );
	double TestVar_All( std::vector< std::vector<double> > v , double avg );
	double TestStd_All( double variance );
	std::vector< std::vector<double> > TestNormalize_All( std::vector< std::vector<double> > v, std::vector<double> avg, double std );

	void TestMatrixNonNegative( std::vector< std::vector<double> > v );
};

class TestInverse
{
public:
	void TestDeterminant( std::vector<std::vector<double> > v, int N );
	void TestCofactor( std::vector< std::vector<double> > v, int N);
};

#endif

