/*
						Stats.h
				Author: Jonathan Christophersen
				Date: 23 November 2014

*/
#ifndef STATS_H
#define STATS_H

#include <vector>
#include <math.h>
#include <iostream>
#include "operations.h"

class Stats
{
public:
	std::vector< std::vector<double> > Option1( std::vector< std::vector<double> > v );
	std::vector< std::vector<double> > Option2( std::vector< std::vector<double> > v );

	std::vector< std::vector<double> > CovarianceMatrix( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 );
	std::vector<double> Average1( std::vector< std::vector<double> > v);
	std::vector<double> Variance1( std::vector< std::vector<double> > v, std::vector<double> avg );
	std::vector<double> StdDev1( std::vector<double> variance );
	std::vector< std::vector<double> > Normalize1( std::vector< std::vector<double> > v, std::vector<double> avg, std::vector<double> std );
	double Average_All( std::vector< std::vector<double> > v);
	double Variance_All( std::vector< std::vector<double> > v, double avg );
	double StdDev_All( double variance );
	std::vector< std::vector<double> > Normalize_All( std::vector< std::vector<double> > v, std::vector<double> avg, double std );

	double Skewness( std::vector< double > v, double avg, double var );
	double Kurtosis( std::vector< double > v, double avg, double var );
	std::vector< double > Anom( std::vector< double > ts, double avg, double var );
	double Median( std::vector< double > A );
	std::vector< double > Biweight( std::vector< double > A );
	std::vector< double > AutoCov( std::vector< double > v, int lag );
	std::vector< double > AutoCorr( std::vector< double > v, int lag, double var );
};

#endif
