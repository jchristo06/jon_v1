#!/bin/bash
#
#	This script is responsible for running the various python scripts in order to calculate
# 	the eigenvalues and eigenvectors from the covariance matrices ('calcPC.py'). This allows us to calculate the 
#	Principal Component scores in 'pcScores.py'. Then, we are able to train the databases in 'pigeonHole.py'.
#

echo " CALCULATING THE PRINCIPAL COMPONENTS "
python calcPC.py

echo " CALCULATING THE PRINCIPAL COMPONENT SCORES "
python pcScores.py

echo " SPANNING THE PRINCIPAL COMPONENT SPACE TO TRAIN DATABASE "
python pigeonHole.py
