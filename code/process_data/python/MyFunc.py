#!/Users/jonathanchristophersen/anaconda/bin/python

import numpy as np
from scipy import stats
from scipy.stats import norm


def getarraydata(fn, minn, maxn):
    ext = ".ascii"
    ls = [fn + repr(i) + ext for i in range(minn, maxn + 1)]

    return ls


def getinitcondarraydata(fn, iminn, imaxn, jminn, jmaxn):
    ls = []
    ext = ".ascii"
    for i in range(iminn, imaxn):
        for j in range(jminn, jmaxn):
            ls.append(fn + repr(i) + "_" + repr(j) + ext)

    return ls


def getfloatdata(fn):
    with open(fn) as f:
        data = map(float, f)

    return data


def getcomplexdata(fn):
    with open(fn) as f:
        data = map(complex, f)

    return data


def gethisto(fn):
    # with open(fn) as f:
    #     data = map(float, f)
    data = np.array(getfloatdata(fn))

    print "Traversing data for NaN values ... "
    data = np.ma.masked_where(np.isnan(data), data)

    print "Getting statistics"
    a = np.ma.mean(data)
    print "Average is done ... ", a
    v = np.ma.var(data)
    print "Variance is done ... ", v
    s = stats.skew(data.compressed())
    print "Skewness is done ... ", s
    k = stats.kurtosis(data.compressed())
    print "Kurtosis is done ... ", k

    maxim = max(data.compressed())
    minim = min(data.compressed())

    print len(data.compressed())

    # Gaussian Reference
    print "Fit Normal distribution curve to the data ... "
    mu, std = norm.fit(data.compressed())
    # sumv=np.sum(SSH)
    # print sumv
    print "Building histogram information ... "
    histo, bins = np.histogram(data.compressed(), bins=20)
    return (histo, bins, mu, std, a, v, s, k, maxim, minim)


def getstatistics(data):
    # Calling to get histogram and statistical information with just an array
    print "Getting statistics"
    a = np.ma.mean(data)
    print "Average is done ... ", a
    v = np.ma.var(data)
    print "Variance is done ... ", v
    s = stats.skew(data.compressed())
    print "Skewness is done ... ", s
    k = stats.kurtosis(data.compressed())
    print "Kurtosis is done ... ", k

    maxim = max(data.compressed())
    minim = min(data.compressed())

    print len(data.compressed())

    # Gaussian Reference
    print "Fit Normal distribution curve to the data ... "
    mu, std = norm.fit(data.compressed())
    # sumv=np.sum(SSH)
    # print sumv
    print "Building histogram information ... "
    histo, bins = np.histogram(data.compressed(), bins=20)
    return (histo, bins, mu, std, a, v, s, k, maxim, minim)
