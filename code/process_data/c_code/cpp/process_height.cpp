/*
  Read PR-AMSU database files with the full 80-bin database format

  NOAA-15/16/17 have AMSUA and AMSUB (89,150,183/1,183/3,183/7)
  NOAA-18/19 and MetOp-A have AMSUA and MHS (89,157,183/1,183/3,190)

  Datasets used to create the matchups have these conventions:
    PR filename syntax: 2A25.20090424.65179.7.HDF
    AMSUB/MHS filename syntax: SWATH_N19_MHS_S09280_052428_E09280_071713.hdf
  so the matchup filenames are derived from these for now.  Can be changed
  to whatever you want.

  -------------------------------------------------------------------------------
  AMSUB/MHS resolution filename syntax:
    2A25.20080412.59305.7.MOA_MHS_S08103_215243_E08103_233534.hdf.bin
  where the first part of the filename is taken from the V7 2A25 file, and the second part is from
  the name of the AMSUB/MHS filename from NESDIS/STAR, where MOA=MetOp-A, and N15, N16, N17, N18, N19 indicate the satellite platform.

  For files at the AMSUB/MHS resolution:  PR profiles are matched to the AMSUB/MHS pixels, taking
  into account the changing AMSUB/MHS FOV shape and orientation.  Then the closest AMSUA pixel is found.
  The time and latitude/longitude in the record are the AMSUB coordinates.
  The sfcrain in the record is the average of the 2A25 surfaceRain for all PR beams that fall inside the AMSUB/MHS pixel.
  The 80-bin mass content profile is computed by interpolating the precipWaterParmA and B values onto the 80-bin correctedZ profile, using
  the ParmNode levels to separate the profile into ice (between levels A and B), mixture ( between levels B and D) and rain ( between levels D and E).
  (using the same method that Sarah Ringerud used).
  This is done for all PR beams that fall into the AMSUB/MHS FOV.  Then all of these 80-bin
  rwc, mwc and iwc profiles that fall inside the AMSUB/MHS FOV are binned to the 28 layers specified in the CSU profile format (500-m spacing up to
  10-km, then 1-km to 18-km).

  A: Echo top (1)
  B: Above Freezing Level (2)
  C: Freezing Level (3)
  D: Below Freezing Level (4)
  E: Near Surface (5) 

  tb1 indices:  0= AMSUA sat_zenith in degrees, 1= AMSUB/MHS-AMSUA time difference in seconds, 2= AMSUB/MHS-AMSUA pixel offset in km
                3-9 are AMSUA channels 1-7.
  tb2 indices:  0= AMSUB sat_zenith in degrees, 1= AMSUB/MHS-PR time difference in seconds, 2= AMSUB/MHS-PR pixel offset in km
                3-7 are AMSUB channels 1-5.

  -------------------------------------------------------------------------------
  AMSUA resolution filename syntax:
    2A25.20080412.59305.7.MOA_AMSUA_S08103_215238_E08103_233526.hdf.bin
  which is the same as above, but the second part is the name of the
  AMSUA filename from NESDIS/STAR, where MOA=MetOp-A, and N15, N16, N17, N18, N19 indicate the satellite platform.

  For files at the AMSUA resolutiion:  Same procedure as above, but the PR profiles are matched to the AMSUA pixels, taking
  into account the changing AMSUA FOV shape and orientation.  Then the closest AMSUB/MHS pixel is found
  and a surrounding 3x3 average is made to get the AMSUB/MHS TB.
  The time and latitude/longitude in the record are the AMSUA coordinates.
  sfcrain, rwc, mwc, and iwc are computed same as above.

  tb1 indices:  0= AMSUA sat_zenith in degrees, 1= AMSUA-PR time difference in seconds, 2= AMSUA-PR pixel offset in km
                3-9 are AMSUA channels 1-7.
  tb2 indices:  0= AMSUB sat_zenith in degrees, 1= AMSUA-AMSUB/MHS time difference in seconds, 2= AMSUA-AMSUB/MHS pixel offset in km
                3-7 are AMSUB channels 1-5.

  -------------------------------------------------------------------------------

  cwc and lh profiles are all set to -99.99


  28 Dec 2011  J. Turk   jturk@jpl.nasa.gov

*/

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <strings.h>
#include <time.h>

using namespace std;

int main(int argc, char *argv[])
{

  FILE *fdb;

  ifstream fileList;
  fileList.open("filelist");

 typedef struct {
    float lat,lon;
    int yyyy,mm,dd,hh,mn,ss;
    int sfc_ab;
    int ray_index_pr, scan_index_ab, scan_index_aa;
    int npr, nrain, nstrat, nconv;
    float satzen_aa, dist_ab_aa, time_ab_aa;
    float satzen_ab, dist_ab_pr, time_ab_pr;
    float sfcrain, sfcrain_nonzero, sfcrain_max;
    float binrain, binrain_nonzero, binrain_max;
    float sigma0, sigma0_min, sigma0_max, freezh;
    float pia, pia_max, loczen_pr;
    float rr_ab;
    float rwc_total, mwc_total, iwc_total;
    float tb_aa[15],tb_ab[5];
    float rwc[80];
    float mwc[80];
    float iwc[80];
    float rwc_max[80];
    float mwc_max[80];
    float iwc_max[80];
    float z[80];
    float zmax[80];
    float rr[80];
    float rr_nonzero[80];
    float rr_max[80];
  } matchup_record;

  int counter = 0;
  int i, j, bytes, nrec=0;
  float ht;
  matchup_record prof;

  // Define in the latitude and longitude 
  float lat_lower = 0.0;
  float lat_upper = 0.0;
  float lon_left = 0.0;
  float lon_right = 0.0;

  /* Create time intervals */
  int month1 = 0;
  int month2 = 0;

  char geoparam[100];
  string rain_str = "rain";
  string ice_str = "ice";
  string mix_str = "mix";
  string clr_str = "clear";

  float threshold = 1.0; // mm/hr
  float ice_threshold = 3.0; // g/m^3

  int rain = 0;
  int rainy = 0;

  float maxht;

  // Initialize two-dimensional array. This is for output in columnar form.
  float X[1000][80]; // State variables - iwc, rwc, mwc. The size of which is ndata x nstate
  float H[10000]; // Highest point of State variables - iwc, rwc, mwc. The size of which is ndata x nstate
  float dummy_ht[80];

  if (argc != 2 ) exit(-1);
  bytes = sizeof(matchup_record);
  /*printf("record size=%d\n", bytes);*/

  if ((fdb = fopen(argv[1],"r")) == 0) {
    printf("Unable to open %s\n", argv[1]);
    exit(-1);
  }

  // Read in the latitude and longitude
  fileList >> lat_lower;
  fileList >> lat_upper;
  fileList >> lon_left;
  fileList >> lon_right;
  fileList >> month1;
  fileList >> month2;
  fileList >> geoparam;


  while ( fread(&prof,bytes,1,fdb) == 1 ) 
  {
    if ( prof.lat >= lat_lower && prof.lat <= lat_upper && prof.lon >= lon_left && prof.lon <= lon_right) // Create lat/lon box
    {
      if (prof.mm >= month1 && prof.mm <= month2) // Seasons
      {
        if ( fabs(prof.satzen_ab) >= 0.0 && fabs(prof.satzen_ab) <= 20.0 ) // Zenith angles intervals to use: 0-20, 20-40, 40+
        { 

          /*              RAIN              */
          if (geoparam == rain_str)
          {
            if ( prof.rwc_total > 0 ) // Detection of "rainy cases"
            {
              for (j=0; j < 80; j++)
              {
                ht = 19.750 - (j*0.250);
                dummy_ht[j] = 19.750 - (j*0.250);
                
                // if (prof.rwc[j] > 0 && maxht <= dummy_ht[j])
                // Threshold is in mm/hr.
                if (prof.rwc[j] > 0 && prof.rr[j] > threshold && dummy_ht[j] >= maxht)
                {
                  maxht = dummy_ht[j];
                }
              }
              printf("%f ", maxht);
              printf("\n");
              printf("\n");
              maxht = 0.0;
              i += 1;
              counter += 1;
            }
          }

          /*              ICE              */
          else if (geoparam == ice_str)
          {
            if ( prof.iwc_total > 0 ) // Detection of "rainy cases"
            {
              for (j=0; j < 80; j++)
              {
                ht = 19.750 - (j*0.250);
                dummy_ht[j] = 19.750 - (j*0.250);
                
                // if (prof.iwc[j] > ice_threshold && prof.rr[j] > threshold && maxht <= dummy_ht[j])
                // if (prof.z[j] >= 17 && maxht <= dummy_ht[j])
                if (prof.iwc[j] > 1.0 && maxht <= dummy_ht[j])
                {
                  maxht = dummy_ht[j];
                }
              }
              printf("%f ", maxht);
              printf("\n");
              printf("\n");
              maxht = 0.0;
              i += 1;
              counter += 1;
            }
          }

          /*              MIX              */
          else if (geoparam == mix_str)
          {
            if ( prof.mwc_total > 0 ) // Detection of "rainy cases"
            {
              for (j=0; j < 80; j++)
              {
                ht = 19.750 - (j*0.250);
                dummy_ht[j] = 19.750 - (j*0.250);
                
                if (prof.mwc[j] > 0 && maxht <= dummy_ht[j])
                {
                  maxht = dummy_ht[j];
                }
              }
              printf("%f ", maxht);
              printf("\n");
              printf("\n");
              maxht = 0.0;
              i += 1;
              counter += 1;
            }
          }

          /*              CLEAR              */
          else if (geoparam == clr_str)
          {
            if ( prof.rwc_total == 0 && prof.iwc_total == 0 && prof.mwc_total == 0 ) // Detection of "clear cases"
            {
              for (j=0; j < 80; j++)
              {
                ht = 19.750 - (j*0.250);
                dummy_ht[j] = 19.750 - (j*0.250);
                
                if (prof.rwc[j] == 0 && maxht <= dummy_ht[j])
                {
                  maxht = dummy_ht[j];
                }
              }
              printf("%f ", maxht);
              printf("\n");
              printf("\n");
              maxht = 0.0;
              i += 1;
              counter += 1;
            }
          }
        }
      }
    }
  }

  // while ( fread(&prof,bytes,1,fdb) == 1 ) 
  // {
  //   if ( prof.lat >= lat_lower && prof.lat <= lat_upper && prof.lon >= lon_left && prof.lon <= lon_right) // Create lat/lon box
  //   {
  //     if (prof.mm >= month1 && prof.mm <= month2) // Seasons
  //     {
  //       if ( fabs(prof.satzen_ab) >= 0.0 && fabs(prof.satzen_ab) <= 20.0 ) // Zenith angles intervals to use: 0-20, 20-40, 40+
  //       { 

  //         /*              RAIN              */
  //         if (geoparam == rain_str)
  //         {
  //           if (rainy == 0)
  //           {
  //             if ( prof.rwc_total == 0 && prof.iwc_total == 0 && prof.mwc_total == 0 ) // Detection of "clear cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.rwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           }

  //           else if (rainy == 1)
  //           {
  //             if ( prof.rwc_total > 0 ) // Detection of "rainy cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.rwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           }
  //         }

  //         /*              ICE              */
  //         else if (geoparam == ice_str)
  //         {
  //           if (rainy == 0)
  //           {
  //             if ( prof.iwc_total == 0 && prof.iwc_total == 0 && prof.mwc_total == 0 ) // Detection of "clear cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.iwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           }

  //           else if (rainy == 1)
  //           {
  //             if ( prof.iwc_total > 0 ) // Detection of "rainy cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.iwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           }
  //         }

  //         /*              MIX              */
  //         else if (geoparam == mix_str)
  //         {
  //           if (rainy == 0)
  //           {
  //             if ( prof.mwc_total == 0 && prof.iwc_total == 0 && prof.mwc_total == 0 ) // Detection of "clear cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.mwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           }

  //           else if (rainy == 1)
  //           {
  //             if ( prof.mwc_total > 0 ) // Detection of "rainy cases"
  //             {
  //               for (j=0; j < 80; j++)
  //               {
  //                 ht = 19.750 - (j*0.250);
  //                 dummy_ht[j] = 19.750 - (j*0.250);
                  
  //                 if (prof.mwc[j] > 0 && maxht <= dummy_ht[j])
  //                 {
  //                   maxht = dummy_ht[j];
  //                   printf("%f ", maxht);
  //                   printf("\n");
  //                   printf("\n");
  //                 }
  //               }
  //               maxht = 0.0;
  //               i += 1;
  //               counter += 1;
  //             }
  //           } 
  //         }
  //       }
  //     }
  //   }
  // }

  // for (i = 0; i < counter; i++)
  // {
  //   for (j = 0; j < 80; j++)
  //   {
  //     printf("%f ", X[i][j]);
  //   }
  //   printf("\n");
  //   printf("\n");
  // }

  fclose(fdb);

  exit(0);

}

