#!/bin/bash
#
#	Clean the directories:
#	../../../files/data/$VAR_mat/
#
#

echo "Cleaning Matrix subfolders"
rm ../../../files/data/Y_mat/*.txt
rm ../../../files/data/X_mat/*.txt
rm ../../../files/data/Y_land_mat/*.txt
rm ../../../files/data/X_land_mat/*.txt
rm ../../../files/data/H_mat/*.txt

