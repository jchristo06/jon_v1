/*
														jon_v1.cpp
												Author: Jonathan Christophersen
													Date: 04 June 2015

			jon_v1 is designed to perform statistical calculations on data that consists of radar and radiometer intersections from AMSU-B radiometer
			and TRMM radar.

			Step 1) Find the average per column, for both Y and X matrices.
			Step 2) Compute scaling constant. Two options:
					(a) Compute the standard deviation per level
					(b) Compute overall sample standard deviation
			Step 3) Calculate the covariance matrices by: 
					(a) Using what is found in steps 1 and 2 to normalize the original matrix, A -> A_norm.
					(b) Taking the transpose of the matrix, A_norm -> A_norm'
					(c) Performing the dot product, A_norm dot A_norm'
					(d) Divide by number of columns in the original matrix, i.e. N_data
*/


#include <iostream>
#include "global.h"
#include "io.h"
#include "stats.h"
#include "operations.h"
#include "test.h"

using namespace std;

int main()
{
	int choice;
	int var_choice;
	int test_choice;

	// Instantiate for access to member functions
	Input read;
	Output write;
	Stats stat;
	VecOps vecops;
	Data dat;
	LinearDiscriminant lindisc;

	cout << "\n";
	cout << "\n";
	cout << "=======================================================================================" << "\n";
	cout << "\n";
	cout << "		Please select from the following options: " << "\n";
	cout << "		1) Select 1 for generation of covariance matrices for the data" << "\n";
	cout << "		2) Select 2 for calculating the linear discriminant" << "\n";
	cout << "		3) Selct 3 for testing the routines" << "\n";
	cout << "\n";
	cout << "=======================================================================================" << "\n";
	cin >> choice;
	cout << "\n";
	cout << "\n";

/*--------------------------------------------- CHOICE 1 *--------------------------------------------- */
	/*
		This choice calculates the covariance matrices for whichever variable you are interested in.
		This is the heart of the calculation for the principal components. Each subchoice in choice == 1
		calculates the autocovariances for both the model state, X, as well as the brightness 
		temperatures, Y, along with the cross-covariance. For clear-sky, the model data is meaningless,
		and therefore only the brightness temperatures (Y space) will be found.
	*/

	if (choice == 1)
	{
		cout << "=======================================================================================" << "\n";
		cout << "		Which variable would you like to analyze?" << "\n";
		cout << "		1) RAIN " << "\n";
		cout << "		2) ICE " << "\n";
		cout << "		3) MIX " << "\n";
		cout << "		4) CLEAR-SKY " << "\n";
		cout << "		5) RAIN OVER LAND " << "\n";
		cout << "		6) ICE OVER LAND " << "\n";
		cout << "		7) MIX OVER LAND " << "\n";
		cout << "		8) CLEAR-SKY OVER LAND " << "\n";
		cout << "		9) HEIGHTS OF PARTICLES " << "\n";
		cout << "		10) TOTAL Tb " << "\n"; 
		cout << "		11) CLOUD WATER MASS " << "\n";
		cout << "=======================================================================================" << "\n";
		cin >> var_choice;

		/*_______________________________________________ RAIN _______________________________________________*/
		if (var_choice == 1)
		{
			cout << " CALCULATING THE MATRICES FOR RAINY DATA " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xr);
			vector< vector<double> > y = read.read2DVector(yr);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_r, Cxx2);
			write.write2DVector(cyy_r, Cyy2);
			write.write2DVector(NormX_r, xPrime);
			write.write2DVector(NormY_r, yPrime);
		}
		/*_______________________________________________ ICE _______________________________________________*/
		else if (var_choice == 2)
		{
			cout << " CALCULATING THE MATRICES FOR ICY DATA " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xi);
			vector< vector<double> > y = read.read2DVector(yi);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_i, Cxx2);
			write.write2DVector(cyy_r, Cyy2);
			write.write2DVector(NormX_i, xPrime);
			write.write2DVector(NormY_i, yPrime);
		}
		/*_______________________________________________ MIX _______________________________________________*/
		else if (var_choice == 3)
		{
			cout << " CALCULATING THE MATRICES FOR MIXED DATA " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xm);
			vector< vector<double> > y = read.read2DVector(ym);
			
			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);
			
			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_m, Cxx2);
			write.write2DVector(cyy_r, Cyy2);
			write.write2DVector(NormX_m, xPrime);
			write.write2DVector(NormY_m, yPrime);
		}
		/*_______________________________________________ CLEAR _______________________________________________*/
		else if (var_choice == 4)
		{
			cout << " CALCULATING THE MATRICES FOR CLEAR-SKY DATA " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > y = read.read2DVector(tbc);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cyy_c, Cyy2);
			write.write2DVector(NormY_c, yPrime);
		}
		/*_______________________________________________ RAIN OVER LAND _______________________________________________*/
		else if (var_choice == 5)
		{
			cout << " CALCULATING THE MATRICES FOR RAINY DATA OVER LAND " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xLr);
			vector< vector<double> > y = read.read2DVector(yLr);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			std::vector< std::vector<double> > Cxy2 = stat.CovarianceMatrix(xPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_l_r, Cxx2);
			write.write2DVector(cyy_l_r, Cyy2);
			write.write2DVector(cxy_l_r, Cxy2);
			write.write2DVector(NormX_l_r, xPrime);
			write.write2DVector(NormY_l_r, yPrime);
		}
		/*_______________________________________________ ICE OVER LAND _______________________________________________*/
		else if (var_choice == 6)
		{
			cout << " CALCULATING THE MATRICES FOR ICY DATA OVER LAND " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xLi);
			vector< vector<double> > y = read.read2DVector(yLi);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			std::vector< std::vector<double> > Cxy2 = stat.CovarianceMatrix(xPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_l_i, Cxx2);
			write.write2DVector(cyy_l_i, Cyy2);
			write.write2DVector(cxy_l_i, Cxy2);
			write.write2DVector(NormX_l_i, xPrime);
			write.write2DVector(NormY_l_i, yPrime);
		}
		/*_______________________________________________ MIX OVER LAND _______________________________________________*/
		else if (var_choice == 7)
		{
			cout << " CALCULATING THE MATRICES FOR ICY DATA OVER LAND " << "\n";
			// Read matrix data - 2D matrices
			vector< vector<double> > x = read.read2DVector(xLm);
			vector< vector<double> > y = read.read2DVector(yLm);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			std::vector< std::vector<double> > Cxy2 = stat.CovarianceMatrix(xPrime, yPrime);

			// OUTPUT to file
			write.write2DVector(cxx_l_m, Cxx2);
			write.write2DVector(cyy_l_m, Cyy2);
			write.write2DVector(cxy_l_m, Cxy2);
			write.write2DVector(NormX_l_m, xPrime);
			write.write2DVector(NormY_l_m, yPrime);
		}
		/*_______________________________________________ CLEAR-SKY OVER LAND _______________________________________________*/
		else if (var_choice == 8)
		{
			cout << " CALCULATING THE MATRICES FOR CLEAR-SKY DATA OVER LAND " << "\n";
			// Read matrix data - 2D matrices
			// vector< vector<double> > x = read.read2DVector(xLc);
			vector< vector<double> > y = read.read2DVector(yLc);

			// Get ride of negative values - set them equal to zero.
			// This will help with the statistical routines.
			// x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);

			// OPTION 2
			// std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);

			// Calculate the COVARIANCE MATRICES:
			// From option 2:
			// std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			// std::vector< std::vector<double> > Cxy2 = stat.CovarianceMatrix(xPrime, yPrime);

			// OUTPUT to file
			// write.write2DVector(cxx_l_c, Cxx2);
			write.write2DVector(cyy_l_c, Cyy2);
			// write.write2DVector(cxy_l_c, Cxy2);
			// write.write2DVector(NormX_l_c, xPrime);
			write.write2DVector(NormY_l_c, yPrime);
		}
		else if (var_choice == 9)
		{
			cout << " CALCULATING THE MATRICES FOR TOP LEVELS " << "\n";
		}
		else if (var_choice == 10)
		{
			cout << " CALCULATING THE MATRICES FOR TOTAL Tb " << "\n";
			std::vector< std::vector<double> > y = read.read2DVector(tbr);
			y = vecops.MatrixNonNegative(y);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			write.write2DVector(Tb_r, Cyy2);
			write.write2DVector(NormTb_r, yPrime);
		}
		else if (var_choice == 11)
		{
			cout << " CALCULATING THE MATRICES FOR CWM " << "\n";
			std::vector< std::vector<double> > x = read.read2DVector(xCwm);
			std::vector< std::vector<double> > y = read.read2DVector(yCwm);
			x = vecops.MatrixNonNegative(x);
			y = vecops.MatrixNonNegative(y);
			std::vector< std::vector<double> > xPrime = stat.Option2(x);
			std::vector< std::vector<double> > yPrime = stat.Option2(y);
			std::vector< std::vector<double> > Cxx2 = stat.CovarianceMatrix(xPrime, xPrime);
			std::vector< std::vector<double> > Cyy2 = stat.CovarianceMatrix(yPrime, yPrime);
			std::vector< std::vector<double> > Cxy2 = stat.CovarianceMatrix(xPrime, yPrime);
			write.write2DVector(cxx_cwm, Cxx2);
			write.write2DVector(cyy_cwm, Cyy2);
			write.write2DVector(cxy_cwm, Cxy2);
			write.write2DVector(NormX_cwm, xPrime);
			write.write2DVector(NormY_cwm, yPrime);
		}
		else
		{
			cout << "YOU CHOSE POORLY ........ " << "\n";
			abort();
		}
	}
/*----------------------------------------------------- Linear Discriminant -----------------------------------------------------*/	
/* For the time being, we are only concerned with finding the discriminant for the rainy cases. Later we'll include the mix and ice cases. */
	else if (choice == 2)
	{
		cout << "Performing Linear Discriminant on the brightness temperatures " << "\n";
		std::vector< std::vector<double> > linearDiscriminant = lindisc.LinDisc();
	}
/*----------------------------------------------------- TEST CASES -----------------------------------------------------*/
	else if (choice == 3)
	{
		cout << "=======================================================================================" << "\n";
		cout << "			1) Select 1 to test the statistics including the covariance matrices 		" << "\n";
		cout << " 			2) Select 2 to test the inverse functions 									" << "\n";
		cout << "=======================================================================================" << "\n";
		cin >> test_choice;

		Testing algTest;
		TestInverse testInv;
		std::vector< std::vector<double> > testMat = read.read2DVector(test);
		algTest.ShowMatrix(testMat);
	
		if (test_choice == 1)
		{
			cout << "-------------------------------------------- OPTION 1 --------------------------------------------" << "\n";	
			std::vector<double> a = algTest.TestAvg(testMat);
			std::vector<double> v = algTest.TestVar(testMat, a);
			std::vector<double> std = algTest.TestStd(v);
			std::vector< std::vector<double> > vprime = algTest.TestNormalize(testMat, a, std);
			std::vector< std::vector<double> > vtrans = algTest.TestMatrixTranspose(vprime);
			std::vector< std::vector<double> > vprod = algTest.TestDotProduct(vprime, vtrans);

			cout << "-------------------------------------------- OPTION 2 --------------------------------------------" << "\n";
			double a2 = algTest.TestAvg_All(testMat);
			double v2 = algTest.TestVar_All(testMat, a2);
			double std2 = algTest.TestStd_All(v2);
			std::vector< std::vector<double> > vprime2 = algTest.TestNormalize_All(testMat, a, std2);
			std::vector< std::vector<double> > vtrans2 = algTest.TestMatrixTranspose(vprime2);
			std::vector< std::vector<double> > vprod2 = algTest.TestDotProduct(vprime2, vtrans2);

			cout << "-------------------------------------------- OTHER --------------------------------------------" << "\n";
			// std::vector< std::vector<double> > newVec = algTest.TestMatrixNonNegative(testMat);
			algTest.TestMatrixNonNegative(testMat);
			algTest.TestCovariance(testMat, testMat); // This functions uses either Option 1 or Option 2!!!!
		}
		else if (test_choice == 2)
		{
			int N = testMat.size();
			testInv.TestDeterminant(testMat, N);
			testInv.TestCofactor(testMat, N);
		}
		else
		{
			cout << "YOU CHOSE POORLY ........ " << "\n";
			abort();
		}
	}
	else
	{
		cout << "YOU CHOSE POORLY ........ " << "\n";
		abort();
	}
}