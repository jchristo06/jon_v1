%------------------------------------------------------------------------------------------------------------------------------------------------%
%|										Preamble										|%
%------------------------------------------------------------------------------------------------------------------------------------------------%

\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage[ametsoc]
%\usepackage[overload]{textcase}
\usepackage{comment}
\usepackage[hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage[pdftex]{graphicx}
\usepackage{caption}
\usepackage{subcaption}
%\linespread{2}
\usepackage[doublespacing]{setspace}
\usepackage{appendix}
\usepackage{float}
\usepackage{natbib}
\usepackage[margin=1in]{geometry}
\bibpunct{(}{)}{;}{a}{}{;}
\renewcommand{\bibname}{References}
\widowpenalty=10000
\clubpenalty=10000

\title{Training of AMSU-B Radiometer for Detection and Retrieval of Cloud Information}
\date{\today}
\author{Jonathan Christophersen}

\begin{document}

\begin{spacing}{1}
\maketitle
\end{spacing}
\newpage


%------------------------------------------------------------------------------------%
%|										Main Body									|%
%------------------------------------------------------------------------------------%
% ------ Intro & Background ------ %
\section{Introduction and Background}\label{sec:intro}

Microwave radiometers are used to gather information about water vapor and condensation in Earth's atmosphere. The process in which these two phases are measured by a radiometer are due to emission/absorption and scattering of radiation, respectively. Vapor absorbs upwelling longwave radiation from Earth's surface and therefore re-emits. That implies that an increase in water mass tends to raise brightness temperatures. However, hydrometeors, such as rain droplets, ice crystals, etc., tend to scatter the upwelling radiation so that an increase in condensation mass tends to lower brightness temperatures. So, for the case of condensed liquid water, both effects happen simultaneously and can make interpretation of measurements from a radiometer difficult.

To combat this caveat, instruments that measure longer wavelength radiation are typically used. Longer wavelengths are used to measure hydrometeors since the individual size of the hydrometeors are relatively small then, compared to the wavelength. To measure larger wavelengths implies that larger and more expensive instruments, with coarse spatial resolution, are used onboard satellites. Hence, there is incentive to study millimeter-wavelength radiometers, which are typically smaller and more cost effective. To the author's knowledge, these instruments have not been used to study precipitation in storms. So, the goal of this study is to use millimeter-wave radiometers to gain insight into the vertical distribution of precipitation rates in clear-sky conditions, in conjunction with radar echoes.

The motivation to derive vertical information from passive radiometers branches from work done to investigate the state of the atmosphere near the region where Air France 447 disappeared in 2009. Ten minutes prior to the disappearance, the Tropical Rainfall Measuring Mission (TRMM) satellite passed over the region. The satellite had a working passive microwave radiometer onboard but did not have an operable radar. Therefore, interpretation of what the atmospheric conditions were like at flight level were muddled. So, by use of prior data from the passive radiometer as well as a coincident and collocated radar, it was found that one could obtain crucial information from the analysis detailed in section~\ref{sec:Obj}.

% ------ Objectives ------ %
\section{Objectives}\label{sec:Obj}

Ideally, one would like to be able to interpret observational data (e.g. brightness temperatures) and derive geophysical quantities (e.g. vertical profiles of rain, ice, etc.). If this were possible, the observations from the radiometer would be enough to determine the vertical profile of precipitation. In the case of Air France, and others like it, the analysis could be done without the slight chance that there is another instrument in the vicinity of the satellite.

Therefore, the purpose of this study is to determine whether or not it is possible and advantageous to develop routines that retrieve geophysical quantities without error-prone assumptions. In doing so, we will train the radiometer directly from previous model states, bypassing the use of radar or other instruments. In spirit the approach is similar to that of \citet{Haddad2009} and \citet{Haddad2010}, but unique in that we attempt to avoid the use of other instruments, namely a radar, and utilize the retrieved geophysical parameters directly.

%The objective of this research is to train the AMSU-B radiometer to retrieve precipitation rates by utilizing the vertical information given by radar echoes from the TRMM satellite. The project will consist of two parts. The first is to analyze model output of simulated storms and their corresponding calculated microwave signatures. The second is to apply the same analysis to millimeter-wave radiances from AMSU-B onboard NOAA-19 along with radar echoes taken during both clear-sky and not-clear conditions in order to teach the radiometer the difference between signals. From there, analysis of the radiometer will continue for cases in which the radar did retrieve vertical information of condensed water. These cases will be used to train the radiometer to retrieve the vertical profiles.

% ------ Methodology ------ %
\section{Methodology}\label{sec:Methodology}

To teach the radiometer how to recognize ice, rain, or mixed conditions, we statistically link the derived state variables (e.g. ice water content, rain water content, or mixed water content) to the brightness temperatures. So, the first task is to create a database of brightness temperatures and the state variables used for analysis. So, define the observational space, matrix {\bf Y} $\in \mathbb{R}^{l \mathrm{x} N_{obs}}$, and the model space, matrix {\bf X} $\in \mathbb{R}^{m \mathrm{x} N_{obs}}$, where $N_{obs}$ is the number of times the radar intersected with the radiometer within a given latitude, longitude, and time constraint and within a certain satellite zenith angle tolerance.

So the question at this stage is: what is the optimal way to functionally map the state space ({\bf X}) to the observation space ({\bf Y})? To do this requires creating a set of orthogonal basis functions by decomposing the databases in terms of their eigenvalues and eigenfunctions. So, the natural first step in answering this question is to compute the auto-covariance and cross-covariance matrices. In general, the covariance measures how two random variables vary with respect to one another. It is mathematically defined as

\begin{equation}\label{eq:Cov}
	C_{jk} = \frac{1}{N-1}\displaystyle \sum_{i=1}^{N}(X_{ij} - \bar{X_{j}})(X_{ik} - \bar{X_{k}}).
\end{equation}
Technically, this is defined as the sample covariance with $N$ observations of $k$ variables. This calculation measures the covariance between variables $j$ and $k$. The covariance matrix is the covariance between multiple variables. In the present case, the covariance matrices are calculated using the mean per level (each row represents a height level in {\bf X}, and channel in {\bf Y}) and the values are normalized to a constant that represents the entire matrix, which was chosen to be the standard deviation over the entire matrix.

From there, the eigenvalues and eigenvectors of the auto-covariance and cross-covariance matrices were found. This yields a set of orthonormal basis functions in which we can regress upon in order to link the two spaces together in a functional way. At this stage, I have not completed the nonlinear regression analysis, though it seems this should be completed within the next week or two.

% ------ Preliminary Results ------ %
\section{Preliminary Results}\label{sec:PreRes}

These are shown below in section~\ref{sec:Figs}. Figure~\ref{fig:Eigenvalues} shows the eigenvalues from the eigenvalue decomposition. Note that the first eigenvalue corresponds to the largest value while the remaining values decrease in succession. The maximum variance corresponds to the first eigenvalue and decreases respectively.

Figures~\ref{fig:EigVec_cxx} and~\ref{fig:EigVec_cyy} show the eigenfunctions from the autocovariance matrices. Figure~\ref{fig:EigVec_cxx} shows the interesting feature that each of the eigenvectors are sensitive to different freezing levels. This is seen by looking at the height levels of the peaks. In figure~\ref{fig:EigVec_cyy}, we see that there is different sensitivity to different channels. So, a combination of channels may be used to give us an optimal correlation for linking the observational space with the state space.

The remaining figures are yet to be analyzed in detail. The next week will be devoted to better understanding of what is featured in these plots.

\section{Summary of What Was Done and What is to Come}\label{sec:Summ}
Significant work has been done to develop the algorithms necessary to read in the data properly, build the databases, calculate the covariance matrices, and perform the eigenvalue (autocovariance) and singular value decomposition (cross-covariance) on these matrices. Along with writing the code to do these routines, testing algorithms were written and carried out, along with analytical calculations for double and triple checking the results. Bugs were found and eliminated so that my results equal those found from other reliable methods.

In summary, I have created two spaces, the observational space, which consists of brightness temperatures given from the AMSU-B radiometer, as well as the model or ``state" space. I performed ``sanity" checks, by plotting certain fields (not shown) and determining whether or not they were correct. From there I decomposed these spaces into their eigenvectors, which are the orthogonal basis functions (referred to in section~\ref{sec:Methodology}). At this point, I am ready to develop the algorithms necessary to link these eigenvectors together using nonlinear regression techniques. Once this is accomplished, testing will be performed to determine how well this technique works against more traditional techniques.

%------------------------------------------------------------------------------------%
%|										References									|%
%------------------------------------------------------------------------------------%

\bibliographystyle{ametsoc}
\begin{spacing}{1}
\bibliography{JonsRefs}
\end{spacing}

\newpage
%------------------------------------------------------------------------------------%
%|										Figures										|%
%------------------------------------------------------------------------------------%
\section{Figures}\label{sec:Figs}

\begin{figure}[h!]
	\centering \includegraphics[width=\textwidth]{../../code/calculations/python/figs/EigVals.pdf}
	\caption{Eigenvalues from the covariance matrices.}
	\label{fig:Eigenvalues}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.9\textwidth}
		\includegraphics[width=\textwidth]{../../code/calculations/python/figs/EigVecs_cxx.pdf}
		\caption{First three principal components of the state space auto-covariance.}
		\label{fig:EigVec_cxx}
	\end{subfigure}

	\begin{subfigure}[b]{0.9\textwidth}
		\includegraphics[width=\textwidth]{../../code/calculations/python/figs/EigVecs_cyy.pdf}
		\caption{First three principal components of the observation space auto-covariance.}
		\label{fig:EigVec_cyy}
	\end{subfigure}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.9\textwidth}
		\includegraphics[width=\textwidth]{../../code/calculations/python/figs/EigVecs_cxy_u.pdf}
		\caption{First three principal components of C'$_{\mathrm{xy}}$C$_{\mathrm{xy}}$.}
		\label{fig:EigVec_cxy_u}
	\end{subfigure}

	\begin{subfigure}[b]{0.9\textwidth}
		\includegraphics[width=\textwidth]{../../code/calculations/python/figs/EigVecs_cxy_v.pdf}
		\caption{First three principal components of C$_{\mathrm{xy}}$C'$_{\mathrm{xy}}$.}
		\label{fig:EigVec_cxy_v}
	\end{subfigure}
\end{figure}


\end{document}
