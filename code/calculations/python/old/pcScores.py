#!/Users/jchristo/anaconda/bin/python
#
#
#			pc_score.py
#		Author: Jonathan Christophersen
#		Date: 22 June 2015
#
#		This program reads in the normalized brightness temperatures produced by 'jon_v1.cpp' and 
#		the principal components found in 'calcPC.py'. Then the inner product is performed
# 		to yield one number for each observation, for each principal component. Finally, the histogram
#		function is called to plot the resulting matrix multiplication.
#
#
#

import matplotlib.pyplot as plt
import pylab
import numpy as np
from numpy import linalg as la

plt.rc('axes', **{'titlesize':12})
plt.rc('legend', **{'fontsize': 11})
plt.rcParams['legend.frameon'] = 'False'

np.set_printoptions(threshold=np.nan)

# Set path and file names:
filePath = "../../../files/data/"
figPath = "./figs/"

# # Input PC scores 1 and 2; paths and file names
# pcscore_1_fn = "pc_scores_1.txt"
# pcscore_2_fn = "pc_scores_2.txt"
# pca_path = str(filePath + "pca/")
# pca_1 = str(pca_path + pcscore_1_fn)
# pca_2 = str(pca_path + pcscore_2_fn)

#------------------------------------------------------------ INPUT DATA ------------------------------------------------------------#
# Input NORMALIZED data
calcPath = str(filePath + "calc_output/")
# X space
xrain = np.genfromtxt(str(calcPath + "NormX_rain.txt"))
xmix = np.genfromtxt(str(calcPath + "NormX_mix.txt"))
xice = np.genfromtxt(str(calcPath + "NormX_ice.txt"))
# Y space
yclear = np.genfromtxt(str(calcPath + "NormY_clear.txt"))
yrain = np.genfromtxt(str(calcPath + "NormY_rain.txt"))
ymix = np.genfromtxt(str(calcPath + "NormY_mix.txt"))
yice = np.genfromtxt(str(calcPath + "NormY_ice.txt"))

# Input PRINCIPAL COMPONENTS
pca_path = str(filePath + "pca/")
# X space
pca_xx_rain = np.genfromtxt(str(pca_path + "pca_xx_rain.txt"))
pca_xx_mix = np.genfromtxt(str(pca_path + "pca_xx_mix.txt"))
pca_xx_ice = np.genfromtxt(str(pca_path + "pca_xx_ice.txt"))
# Y space
pca_yy_clear = np.genfromtxt(str(pca_path + "pca_yy_clear.txt"))
pca_yy_rain = np.genfromtxt(str(pca_path + "pca_yy_rain.txt"))
pca_yy_mix = np.genfromtxt(str(pca_path + "pca_yy_mix.txt"))
pca_yy_ice = np.genfromtxt(str(pca_path + "pca_yy_ice.txt"))

#------------------------------------------------------------ OUTPUT DATA ------------------------------------------------------------#
# Output data paths and file names
pca_path = "../../../files/data/pca/"
# X space
pcscore_xx_r = str(pca_path + "pcScores_xx_rain.txt")
pcscore_xx_m = str(pca_path + "pcScores_xx_mix.txt")
pcscore_xx_i = str(pca_path + "pcScores_xx_ice.txt")
# Y space
pcscore_yy_c = str(pca_path + "pcScores_yy_clear.txt")
pcscore_yy_r = str(pca_path + "pcScores_yy_rain.txt")
pcscore_yy_m = str(pca_path + "pcScores_yy_mix.txt")
pcscore_yy_i = str(pca_path + "pcScores_yy_ice.txt")

#---------------------------------------------- CALCULATE PRINCIPAL COMPONENT SCORES ----------------------------------------------#
# X space
pcScore_xx_r = np.dot(pca_xx_rain, xrain)
pcScore_xx_m = np.dot(pca_xx_mix, xmix)
pcScore_xx_i = np.dot(pca_xx_ice, xice)
# Y space
pcScore_yy_c = np.dot(pca_yy_clear, yclear)
pcScore_yy_r = np.dot(pca_yy_rain, yrain)
pcScore_yy_m = np.dot(pca_yy_mix, ymix)
pcScore_yy_i = np.dot(pca_yy_ice, yice)

# Break into two components
# Create the 'feature vector' in order to calculate the principal components
N = 2 # Keep the first N eigenvectors for our new data.
# X space
pcScore_xx_rain = pcScore_xx_r[0, :]
pcScore_xx_mix = pcScore_xx_m[0, :]
pcScore_xx_ice = pcScore_xx_i[0, :]
for i in range(1, N):
	pcScore_xx_rain = np.vstack((pcScore_xx_rain, pcScore_xx_r[i, :]))
	pcScore_xx_mix = np.vstack((pcScore_xx_mix, pcScore_xx_m[i, :]))
	pcScore_xx_ice = np.vstack((pcScore_xx_ice, pcScore_xx_i[i, :]))

# Y space
pcScore_yy_clear = pcScore_yy_c[0, :]
pcScore_yy_rain = pcScore_yy_r[0, :]
pcScore_yy_mix = pcScore_yy_m[0, :]
pcScore_yy_ice = pcScore_yy_i[0, :]
for i in range(1, N):
	pcScore_yy_clear = np.vstack((pcScore_yy_clear, pcScore_yy_c[i, :]))
	pcScore_yy_rain = np.vstack((pcScore_yy_rain, pcScore_yy_r[i, :]))
	pcScore_yy_mix = np.vstack((pcScore_yy_mix, pcScore_yy_m[i, :]))
	pcScore_yy_ice = np.vstack((pcScore_yy_ice, pcScore_yy_i[i, :]))
#------------------------------------------ OUTPUT SECTION ------------------------------------------#
print 
print " Saving to ", pcscore_xx_r, "  ", pcscore_xx_m, "  ", pcscore_xx_i
np.savetxt(pcscore_xx_r, pcScore_xx_rain)
np.savetxt(pcscore_xx_m, pcScore_xx_mix)
np.savetxt(pcscore_xx_i, pcScore_xx_ice)
print 
print " Saving to ", pcscore_yy_c, "  ", pcscore_yy_r, "  ", pcscore_yy_m, "  ", pcscore_yy_i
np.savetxt(pcscore_yy_c, pcScore_yy_clear)
np.savetxt(pcscore_yy_r, pcScore_yy_rain)
np.savetxt(pcscore_yy_m, pcScore_yy_mix)
np.savetxt(pcscore_yy_i, pcScore_yy_ice)
print 
#------------------------------------ PLOTTING SECTION ------------------------------------#
# pc1Score_name1 = "pc1Score_yy_rain.pdf"
# pc2Score_name1 = "pc2Score_yy_rain.pdf"
# pc1Score_name2 = "pc1Score_yy_clear.pdf"
# pc2Score_name2 = "pc2Score_yy_clear.pdf"
# pc_clearScat_name = "PC_Scores_clear.eps"
# pc_rainScat_name = "PC_Scores_rain.eps"
# pc1_clear_rain_Scat_name = "PC1_Scores_clear_rain.pdf"
# pc2_clear_rain_Scat_name = "PC2_Scores_clear_rain.pdf"
# pc1ScoreFn1 = str(figPath + pc1Score_name1)
# pc2ScoreFn1 = str(figPath + pc2Score_name1)
# pc1ScoreFn2 = str(figPath + pc1Score_name2)
# pc2ScoreFn2 = str(figPath + pc2Score_name2)
# pc_clearScat_Fn = str(figPath + pc_clearScat_name)
# pc_rainScat_Fn = str(figPath + pc_rainScat_name)
# pc1_clear_rain_Scat_Fn = str(figPath + pc1_clear_rain_Scat_name)
# pc2_clear_rain_Scat_Fn = str(figPath + pc2_clear_rain_Scat_name)

#------------- HISTOGRAMS -------------#
# plt.hist(pcScore1_rain, bins=25)
# plt.xlabel("PC scores")
# plt.ylabel("Frequency")
# plt.title("Principal Component Scores for the First Principal Component", fontsize=10)
# plt.savefig(pc1ScoreFn1)
# plt.close()

# plt.hist(pcScore2_rain, bins=25)
# plt.xlabel("PC scores")
# plt.ylabel("Frequency")
# plt.title("Principal Component Scores for the Second Principal Component", fontsize=10)
# plt.savefig(pc2ScoreFn1)
# plt.close()

# plt.hist(pcScore1_clear, bins=25)
# plt.xlabel("PC scores")
# plt.ylabel("Frequency")
# plt.title("Principal Component Scores for the First Principal Component", fontsize=10)
# plt.savefig(pc1ScoreFn2)
# plt.close()

# plt.hist(pcScore2_clear, bins=25)
# plt.xlabel("PC scores")
# plt.ylabel("Frequency")
# plt.title("Principal Component Scores for the Second Principal Component", fontsize=10)
# plt.savefig(pc2ScoreFn2)
# plt.close()

#------------- SCATTER PLOTS -------------#
# fig = plt.figure()
# ax = fig.add_subplot(111)
# handles = []
# props = dict(alpha = 0.75, edgecolors='none')
# ax.scatter(pcScore1_clear, pcScore2_clear, color=["b", "r"])
# ax.grid(True)
# plt.xlabel("PC Scores for Clear-Sky for First Eigenvalue")
# plt.ylabel("PC Scores for Clear-Sky for Second Eigenvalue")
# plt.savefig(pc_clearScat_Fn)
# plt.close()

# fig = plt.figure()
# ax = fig.add_subplot(111)
# props = dict(alpha = 0.75, edgecolors='none')
# im = ax.scatter(pcScore1_rain, pcScore2_rain)
# ax.grid(True)
# plt.xlabel("PC Scores for Rain for First Eigenvalue")
# plt.ylabel("PC Scores for Rain for Second Eigenvalue")
# plt.savefig(pc_rainScat_Fn)
# plt.close()

# fig = plt.figure()
# ax = fig.add_subplot(111)
# props = dict(alpha = 0.5, edgecolors='none')
# handles = []
# colors = rainyY[0,:]
# im = ax.scatter(subset_Clear1, pcScore1_rain, c=colors, cmap=plt.cm.jet, **props)
# cb = fig.colorbar(im, ax=ax)
# cb.set_label("Brightness temperatures")
# ax.grid(True)
# plt.xlabel("PC Scores for Clear-Sky for First Eigenvalue")
# plt.ylabel("PC Scores for Rain for First Eigenvalue")
# plt.savefig(pc1_clear_rain_Scat_Fn)
# plt.close()

# fig = plt.figure()
# ax = fig.add_subplot(111)
# props = dict(alpha = 0.5, edgecolors='none')
# handles = []
# colors = rainyY[0,:]
# im = ax.scatter(subset_Clear2, pcScore2_rain, c=colors, cmap=plt.cm.jet, **props)
# cb = fig.colorbar(im, ax=ax)
# cb.set_label("Brightness temperatures")
# ax.grid(True)
# plt.xlabel("PC Scores for Clear-Sky for Second Eigenvalue")
# plt.ylabel("PC Scores for Rain for Second Eigenvalue")
# plt.savefig(pc2_clear_rain_Scat_Fn)
# plt.close()

