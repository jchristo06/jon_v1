#!/Users/jchristo/anaconda/bin/python
#
#		create_database.py
#	Author: Jonathan Christophersen
#	Data: 04 June 2015
#
#	This python script is responsible for creating a coherent database for the intersection data of AMSU-B radiometer and TRMM radar. The 
#	data was processed and put into files by read_PR_MHS_full.cpp. This script takes the data within those files and puts it together into
#	a matrix.
#
#	This particular python script is for the WestPac durfing JJA season.
#

# import modules
import matplotlib.pyplot as plt
import numpy as np
import os
import glob

# TeX fonts
plt.rc('text', usetex=True)
plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
plt.rc('axes', **{'titlesize':12})
plt.rc('legend', **{'fontsize': 11})
plt.rcParams['legend.frameon'] = 'False'

# Build strings
ending = ".ascii"
year1 = "2009"
year2 = "2010"
filePath = "../../../files/"
filePath_python = "../../code/process_data/python"
filePath_2009 = str(filePath + year1 + "/")
filePath_2010 = str(filePath + year2 + "/")

#-------------------------------- Work on matrix for the 2009 data --------------------------------#
# Change directory to the 2009 file folder
os.chdir(filePath_2009)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(85)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2009 data ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath_2009 + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == 85:
		f = f.reshape(1, 85)

	# print "The array shape for ", data1, "for i = ", i, "is ", f.shape, "\n"
	if i == 0:
		mat2009 = f

	if i > 0:
		mat2009 = np.concatenate((mat2009, f))
		# print "The new array's shape is ", mat2009.shape, "\n"

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)


#-------------------------------- Work on matrix for the 2010 data --------------------------------#
# Change directory to the 2010 file folder
os.chdir(filePath_2010)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(85)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the 2010 data ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath_2010 + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == 85:
		f = f.reshape(1, 85)

	# print "The array shape for ", data1, "for i = ", i, "is ", f.shape, "\n"
	if i == 0:
		mat2010 = f

	if i > 0:
		mat2010 = np.concatenate((mat2010, f))
		# print "The new array's shape is ", mat2010.shape, "\n"


print "Putting 2009 and 2010 matrices together ......... ", "\n"
mat = np.concatenate((mat2009, mat2010))
mat = mat.T
print mat.shape
