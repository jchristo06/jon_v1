#!/Users/jchristo/anaconda/bin/python
#
#
#			calculate_svd.py
#		Author: Jonathan Christophersen
#		Date: 10 June 2015
#
#	This program takes covariance matrix output from "../c_code/jon_v1.cpp" and decomposes them into
#	A = USV*. Here, U and V are unitary matrices (def: square matrix, such that U*U = UU* = I, where U* is the conjugate 
#	transpose matrix and I is the identity matrix) and S is diagonal.
#
#	Also, we will get the eigenvalues and eigenvectors from the convariance matrices. These are the principal components.
#	
#

import matplotlib.pyplot as plt
import pylab
import numpy as np
from numpy import linalg as la

plt.rcParams['legend.frameon'] = 'False'

np.set_printoptions(threshold=np.nan)

# Input data paths and file names
filePath = "../../../files/data/calc_output/"

# Output data paths and file names
pca_path = "../../../files/data/pca/"

variable = raw_input('Please enter which variable you would like to analyze? rain, ice, mix, clear, cwm, or Total_Tb_rainy:   ')

if variable == 'rain' or variable == 'ice' or variable == 'mix' or variable == 'cwm':
	Cxx_var_Path = str(filePath + "Cxx_" + variable + ".txt")
	Cyy_var_Path = str(filePath + "Cyy_" + variable + ".txt")
	Cxy_var_Path = str(filePath + "Cxy_" + variable + ".txt")
	Cxx = np.genfromtxt(Cxx_var_Path)
	Cyy = np.genfromtxt(Cyy_var_Path)
	Cxy = np.genfromtxt(Cxy_var_Path)
	Eigxx_fn = str(pca_path+ "lambdaxx_" + variable + ".txt")
	Eigyy_fn = str(pca_path + "lambdayy_" + variable + ".txt")
	pca_xx_fn = str(pca_path + "pca_xx_" + variable + ".txt")
	pca_yy_fn = str(pca_path + "pca_yy_" + variable + ".txt")
elif variable == 'clear':
	Cyy_var_Path = str(filePath + "Cyy_" + variable + ".txt")
	Cyy = np.genfromtxt(Cyy_var_Path)
	Eigyy_fn = str(pca_path + "lambdayy_" + variable + ".txt")
	pca_yy_fn = str(pca_path + "pca_yy_" + variable + ".txt")
elif variable == 'Total_Tb_rainy':
	Cyy_var_Path = str(filePath + "Total_Tb_rainy.txt")
	Cyy = np.genfromtxt(Cyy_var_Path)
	Eigyy_fn = str(pca_path + "lambda_" + variable + ".txt")
	pca_yy_fn = str(pca_path + "pca_Tb_" + variable + ".txt")
else:
	print "YOU CHOSE POORLY ..... "
	exit()

print Cxx.shape
# Get Eigenvalues and Eigenvectors of autocovariances
# Since autocovariance matrices are square, we can apply
# regular eigenvalue decomposition.
if variable != 'clear' and variable != 'Total_Tb_rainy':
	[Uxx, Sxx, Vxx] = la.svd(Cxx)
	[Uyy, Syy, Vyy] = la.svd(Cyy)
	# [eigVal_xx, eigVec_xx] = la.eig(Cxx)
	# [eigVal_yy, eigVec_yy] = la.eig(Cyy)
	# Get SVD matrices for cross-covariance
	# Since cross-covariance matrices are rectangular, we can not
	# apply regular eigenvalue decomposition, but we must perform
	# the Singular Value Decomposition (SVD) on the matrices to get
	# eigenvectors for C'C (Uxy) and CC' (Vxy).
	[Uxy, Sxy, Vxy] = la.svd(Cxy)
	# Verify that the principal components (i.e. the eigenvectors of the covariance matrices) are in fact
	# orthonormal. That is, Ai' * Aj = 0 for i != j and Ai'*Ai = 1. Therefore we should see that the
	# diagonal components are equal to 1 and the off-diagonal components are 0.
	dottedEigVec_xx = np.dot(Uxx.T, Uxx)
	dottedEigVec_yy = np.dot(Uyy.T, Uyy)
	dottedUxy = np.dot(Uxy.T, Uxy)
	sz_xx = dottedEigVec_xx.shape
	sz_yy = dottedEigVec_yy.shape
	sz_xy = dottedUxy.shape
	# Create the 'feature vector' in order to calculate the principal components
	N = 3 # Keep the first N eigenvectors for our new data.
	PCA_xx = Uxx[:, 0]
	PCA_yy = Uyy[:, 0]
	for i in range(1, N):
		PCA_xx = np.vstack( (PCA_xx, Uxx[:, i]) )
		PCA_yy = np.vstack( (PCA_yy, Uyy[:, i]) )
	#------------------------------------------ OUTPUT SECTION ------------------------------------------#
	print 
	print " Saving to ", Eigxx_fn, " and ", Eigyy_fn
	np.savetxt(Eigxx_fn, Sxx)
	np.savetxt(Eigyy_fn, Syy)
	print 
	print " Saving to ", pca_xx_fn, " and ", pca_yy_fn
	np.savetxt(pca_xx_fn, PCA_xx)
	np.savetxt(pca_yy_fn, PCA_yy)
	print 
# The clear-sky scenario only makes sense for brightness temperatures. The covariance of the model state, X, would all be 0 or NaN.
elif variable == 'clear' or variable == 'Total_Tb_rainy':
	[Uyy, Syy, Vyy] = la.svd(Cyy)
	# [eigVal_yy, eigVec_yy] = la.eig(Cyy)
	dottedEigVec_yy = np.dot(Uyy.T, Uyy)
	sz_yy = dottedEigVec_yy.shape
	N = 3 # Keep the first N eigenvectors for our new data.
	PCA_yy = Uyy[:, 0]
	for i in range(1, N):
		PCA_yy = np.vstack((PCA_yy, Uyy[:, i]))
	#------------------------------------------ OUTPUT SECTION ------------------------------------------#
	print 
	print " Saving to ", Eigyy_fn
	np.savetxt(Eigyy_fn, Syy)
	print 
	print " Saving to ", pca_yy_fn
	np.savetxt(pca_yy_fn, PCA_yy)
	print 
