/*
				Operations.cpp
*/

#include "operations.h"


/*																	Data Class																	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/



// Low pass filter
std::vector< double > Data::lowPass( std::vector< double > input )
{
	int i = 0;
	double dt = 0.5;
	double RC = 0.5;
	double alpha = dt/(RC + dt);
	double dummy = 0.0;
	std::vector< double > output = input;

	for (i = 1; i < input.size(); i++)
	{
		if (output[i] != 0)
		{
			dummy = output[i-1] + alpha * ( input[i] - output[i-1] );
			output[i] = dummy;
		}
	}

	return output;
}

std::vector< double > Data::Smoothing( std::vector< double > v )
{
	int i = 0;
	int sz = v.size();
	double dummy = 0.0;

	std::vector<double> S = v;

	for (i = 2; i < sz-1; i++)
	{
		if (S[i] != 0)
		{
			dummy = (v[i-2] + 2 * v[i-1] + 3 * v[i] + 2 * v[i+1] + v[i+2]) / 9;
			S[i] = dummy;
		}
	}

	return S;
}


int Data::Partition( std::vector< double >& A, int p, int q )
{
	double x = A[p];
	int i = p;
	int j;

	for (j = p+1; j < q; j++)
	{
		if (A[j] <= x)
		{
			i = i+1;
			std::swap(A[i], A[j]);
		}
	}

	std::swap(A[i], A[p]);

	return i;
}

std::vector< double > Data::QuickSort( std::vector< double >& A, int p, int q )
{
	int r;

	if (p < q)
	{
		r = Data::Partition(A, p, q);
		QuickSort(A, p, r);
		QuickSort(A, r+1, q);
	}

	return A;
}


std::vector< double > Data::Resize1D( std::vector<double> v, int min, int max )
{
	std::vector<double> v_out;

	for (int i = min; i < max; i++)
	{
		v_out.push_back(v[i]);
	}

	return v_out;
}

std::vector< std::vector<double> > Data::Resize2D( std::vector< std::vector<double> > v, int row_min, int row_max, int col_min, int col_max )
{
	int row_size = row_max - row_min;
	int col_size = col_max - col_min;
	std::vector< std::vector<double> > v_out(row_size, std::vector<double> (col_size));

	std::cout << "Trimming matrix ..... " << "\n";
	std::cout << "\n";
	std::cout << "\n";
	for (int i = row_min; i < row_max; i++)
	{
		for (int j = col_min; j < col_max; j++)
		{
			v_out[i - row_min][j - col_min] = v[i][j];
		}
	}

	return v_out;
}


/*																	Matrix Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* This requires a 2-dimensional matrix. For the 1-dimensional case use the OuterProduct() (coming soon...). Input to the function are two 2D vectors that
	must be of sizes: v1 ~ n x m, v2 ~ m x l. The product should then yield a matrix of size n x l.
*/ 

/* This transposes N x M matrices to M x N matrices */
std::vector< std::vector<double> > VecOps::MatrixTranspose( std::vector< std::vector<double> > v )
{
	// Get dimensions from original vector
	int rows = v.size();
	int cols = v[0].size();
	// Set dimensions for new vector
	std::vector< std::vector<double> > v1(cols, std::vector<double> (rows));	

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			v1[j][i] = v[i][j];
		}
	}

	return v1;
}

/* This transposes N x 1 matrices to 1 x N matrices */
std::vector< std::vector<double> > VecOps::VectorTranspose( std::vector<double> v )
{
	// Get dimensions from original vector
	int rows = v.size();

	// Set dimensions for new vector
	std::vector< std::vector<double> > v1(1, std::vector<double> (rows));

	for (int i = 0; i < rows; i++)
	{
		v1[0][i] = v[i];
	}

	return v1;
}

std::vector< std::vector<double> > VecOps::DotProduct( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 )
{
	// Get rows from v1 & v2
	int rows_v1 = v1.size();
	int rows_v2 = v2.size();
	// Get columns from v1 & v2
	int cols_v1 = v1[0].size();
	int cols_v2 = v2[0].size();
	// Set new matrix dimensions
	std::vector< std::vector<double> > v(rows_v1, std::vector<double> (cols_v2));

	if (cols_v1 == rows_v2)
	{
		for (int i = 0; i < rows_v1; i++)
		{
			for (int j = 0; j < cols_v2; j++)
			{
				for (int k = 0; k < rows_v2; k++)
				{
					v[i][j] = v[i][j] + v1[i][k] * v2[k][j];
				}
			}
		}
	}
	else
	{
		std::cout << "Incompatible dimension sizes ... " << "\n";
		exit(-1);
	}

	return v;
}

std::vector<double> VecOps::VectorSubtract( std::vector<double> v1, std::vector<double> v2 )
{
	// Get rows from v1 & v2
	int rows_v1 = v1.size();
	int rows_v2 = v2.size();

	// Set new vector dimensions
	std::vector<double> v(rows_v1);

	if (rows_v1 == rows_v2)
	{
		for (int i = 0; i < rows_v1; i++)
		{
			v[i] = v1[i] - v2[i];
		}
	}
	else
	{
		std::cout << "Incompatible dimension sizes ... " << "\n";
		exit(-1);
	}

	return v;
}

std::vector< std::vector<double> > VecOps::MatrixAddition( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 )
{
	// Get rows from v1 & v2
	int rows_v1 = v1.size();
	int rows_v2 = v2.size();
	// Get cols from v1 & v2
	int cols_v1 = v1[0].size();
	int cols_v2 = v2[0].size();

	if (rows_v1 == rows_v2 && cols_v1 == cols_v2)
	{
		std::vector< std::vector<double> > v(rows_v1, std::vector<double> (cols_v1));

		for (int i = 0; i < rows_v1; i++)
		{
			for (int j = 0; j < cols_v1; j++)
			{
				v[i][j] = v1[i][j] + v2[i][j];
			}
		}

		return v;
	}
	else
	{
		std::cout << "Incompatible dimension sizes ... " << "\n";
		exit(-1);
	}
}

std::vector< std::vector<double> > VecOps::MatrixNonNegative( std::vector< std::vector<double> > v)
{
	std::vector< std::vector<double> > v2(v.size(), std::vector<double> (v[0].size()));

	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j < v[i].size(); j++)
		{
			if (v[i][j] > 0.0)
				v2[i][j] = v[i][j];
			else
				v2[i][j] = 0.0;
		}
	}

	return v2;
}

std::vector< std::vector<double> > VecOps::ScalarProduct( double a, std::vector< std::vector<double> > v )
{
	std::vector< std::vector<double> > v2(v.size(), (std::vector<double> (v[0].size())));
	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j < v[0].size(); j++)
		{
			v2[i][j] = a * v[i][j];
		}
	}

	return v2;
}

/*														Linear Discriminant Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*
		Following the formula: (mur - muc)^(t) . (Cr + Cc)^(-1) x
		where mur and muc are the averages of the brightness temperatures (5x1) for the rainy and clear cases, respectively.
		The Cr and Cc are the covariance matrices of the brightness temperatures (5x5) for the rainy and clear cases, respectively.
		Here, x is a vector that consists of terms that originate from the databases of raw data found from processing.
*/
std::vector< std::vector<double> > LinearDiscriminant::LinDisc()
{
	Input read;
	Stats stat;
	VecOps ops;
	MatrixInverse inv;

	std::cout << " Read in Brightness Temperatures for the rainy cases " << "\n";
	std::vector< std::vector<double> > Yc = read.read2DVector(yc);
	std::vector< std::vector<double> > Yr = read.read2DVector(yr);

	std::cout << " Read in the Covariance Matrices " << "\n";
	std::vector< std::vector<double> > Cc = read.read2DVector(cyy_c);
	std::vector< std::vector<double> > Cr = read.read2DVector(cyy_r);

	std::cout << " Calculating the Averages of Brightness Temperatures " << "\n";
	std::vector<double> mu_c = stat.Average1(Yc);
	std::vector<double> mu_r = stat.Average1(Yr);

	// Take the difference between the averages.
	std::vector<double> mu_d = ops.VectorSubtract(mu_r, mu_c);

	// Take the transpose of the difference of the averages
	std::vector< std::vector<double> > mu_d_prime = ops.VectorTranspose(mu_d);

	// Add Ccovariance matrices together
	std::vector< std::vector<double> > Cadd = ops.MatrixAddition(Cc, Cr);

	// Invert the added covariance matrix
	std::vector< std::vector<double> > Cadd_invert = inv.matrixInverse(Cadd);

	// Take the dot product of the subtracted and transposed averages with the inverted and added covariance matrices
	// std::vector< std::vector<double> > 
}


/*															Matrix Inverse Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*
		The inverse matrix is defined as A^(-1) = (1/det(A)) * adj(A), where det(A) is the determinant
		of matrix A and adj(A) is the adjoint of matrix A. The adjoint matrix is defined as the transpose of
		the cofactor matrix. The cofactor matrix is defined as the determinants of the minors multiplied by (-1)^(i+j).
		Mathematically, it is: (-1)^(i+j) * det(A_ij). The minors are determinants of some smaller square matrix
		within A.
		Steps to get the inverse matrix:
		1) Find the cofactor matrix by finding determinants of the minors
		   in matrix v
		2) Transpose cofactor matrix to give the adjoint matrix
		3) Find the determinant of the entire matrix so as to divide 
		   the adjoint matrix.
*/
std::vector< std::vector<double> > MatrixInverse::matrixInverse(std::vector< std::vector<double> > v)
{
	VecOps ops;
	int N = v.size();

	std::vector< std::vector<double> > cofactor = CoFactor(v, N);
	std::vector< std::vector<double> > adjoint = ops.MatrixTranspose(cofactor);
	double det = Determinant(v, N);
	double detInverse = 1.0/det;

	std::vector< std::vector<double> > v_inverse = ops.ScalarProduct(detInverse, adjoint);

	return v_inverse;
}

double MatrixInverse::Determinant(std::vector< std::vector<double> > v, int N)
{
	double det = 0.0;

	// Set new matrix dimensions -> SQUARE MATRIX
	std::vector< std::vector<double> > v2(N, std::vector<double> (N));

	if (N == 1)
	{
		det = v[0][0];
	}
	else if (N == 2)
	{
		det = (v[0][0] * v[1][1]) - (v[1][0] * v[0][1]);
	}
	else
	{
		det = 0.0;
		for (int j1 = 0; j1 < N; j1++)
		{
			for (int i = 1; i < N; i++)
			{
				int j2 = 0;
				for (int j = 0; j < N; j++)
				{
					if (j == j1)
					{
						continue;
					}
					v2[i-1][j2] = v[i][j];
					j2++;
				}
			}
			det += pow(-1.0, j1 + 2.0) * v[0][j1] * Determinant(v2, N - 1);
		}
	}

	return det;
}

// Find the determinants of the minors
std::vector< std::vector<double> > MatrixInverse::CoFactor(std::vector< std::vector<double> > v, int N)
{
	// Set new matrix dimensions -> SQUARE MATRIX
	std::vector< std::vector<double> > v2(N, std::vector<double> (N));
	std::vector< std::vector<double> > cofactor(N, std::vector<double> (N));

	for (int j = 0; j < N; j++)
	{
		for (int i = 0; i < N; i++)
		{
			/* Form the adjoint v_ij */
			int i1 = 0;
			for (int ii = 0; ii < N; ii++)
			{
				if (ii == i)
				{
					continue;
				}
				int j1 = 0;
				for (int jj = 0; jj < N; jj++)
				{
					if (jj == j)
					{
						continue;
					}
					v2[i1][j1] = v[ii][jj];
					j1++;
				}
				i1++;
			}
			/* Calculate the determinant */
			double det = Determinant(v2, N - 1);

			/* Fill in elements for cofactor */
			cofactor[i][j] = pow(-1.0, i + j + 2.0) * det;
		}
	}

	return cofactor;
}

















