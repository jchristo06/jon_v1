# README #

This README will discuss what this repository is for and how to get it up and running properly. I will list the steps necessary for meaningful results.

### Summary ###

This repository contains code to:
   
* a) Read data from the MET-OP radiometer on board the TRMM satellite and the AMSU-B radar on board NOAA-19 during
      times when the two satellites were near one another. The data covers the entire globe over a span of two years.
   
* b) Output various parameters to specific files in a particular directory tree structure. This will be discussed 
      below, but briefly I've separated the variables into two overall vector spaces: 
                  X - the state variables (geophysical parameters such as ice, rain, etc.)
                  Y - the brightness temperatures
      Other variables include the top level at which ice, rain, etc. occur, the total amount of ice, rain, etc. in
      the column, and so on. This is handled in '/jon_V1/code/process_data/c_code/'. The output of these files is 
      held in '/jon_V1/files/$VAR', where $VAR is the variable in question. UPDATE (1 July 2015): use 'sh run.sh' to process the
      data. Don't forget to change the variable in the run.sh script.

   
* c) Next, the directory '/jon_V1/code/process_data/python/' contains python scripts that piece together the output
      files into coherent matrices. They are separated by which space they are to work on. For example, 
      'build_X_matrix.py' is responsible for building the matrix of geophysical quantities output from the c_code
      directory. This matrix is then output to '/jon_V1/files/data/X_mat/' as a text file. UPDATE (1 July 2015): use 'sh run.sh' to
      build the matrices.
   
* d) Once the matrices are built, the directory '/jon_V1/code/calculations/c_code/' holds the code responsible for
      the heavy calculation of the covariance matrices. So, the code reads in the files in '/jon_V1/files/data/X_mat/'
      or '/jon_V1/files/data/Y_mat/', etc. and performs various routines to normalize the data (which is also 
      output) and then calculate the auto-covariance (C_xx, C_yy) and cross-covariance (C_xy). Also are testing
      routines that display tests performed on a test matrix built by
      '/jon_V1/code/process_data/python/build_test_matrix.py'. This is usually a simple matrix but is sufficient for
      our purposes.
      The matrices (covariances and normalized) are output to '/jon_V1/files/data/calc_output/'.
   
* e) After that, '/jon_V1/code/calculations/python/calculate_svd.py' calculates the eigenvalues and eigenfunctions
      of the covariance matrices calculated from the code mentioned above. These are the principal components.
      The data is output to '/jon_V1/files/data/pca/'. UPDATE (1 July 2015): 'calculate_svd.py' is now 'calcPc.py'.

* f) Next, '/jon_V1/code/calculations/python/pc_scores.py' calculates the Principal Component scores for the first 
      two principal components. This is done by computing the inner product of the eigenfunctions (calculate_svd.py) 
      with the normalized data from the c++ code from part d). The output is placed in '/jon_V1/files/data/pca/' as 
      well. UPDATE (1 July 2015): 'pc_scores.py' is now 'pcScores.py'. This is obviously a major update ;-)
   
* g) Currently, a python script is being written that creates a set of bins of PC scores by finding the max and min 
      of PC scores from Y and slicing the set up in to N bins. A pigeonhole process is carried out that finds the 
      index such that the PC scores in Y matches the PC scores from the generated set. Using that index, we can locate
      the corresponding physical characteristics from space X and place those in the binned set. Once that is carried 
      out, we are able to then find the average, variance, etc. within each bin. UPDATE (1 July 2015): 'build_density_matrix.py' is 
      now 'pigeonHole.py'. Also, use 'sh run.sh' to run programs e-g.

### How do I get set up? ###

First thing to do is get the data from either myself or Joe Turk. Placing it in '/Users/$HOME/AH5017/N19/$YEAR/'
is the way that I've done it. This requires you to make sure you put 001-620 in '/2009/' and 621-989 in '/2010/'. 
I renamed the files using a shell script to sequential numbers, 001 - 989. That's what is used in the shell scripts 
in '/jon_V1/code/process_data/c_code/.sh'. I use a variable, COUNT in the shell files to iterate through the names 
and read those files into the corresponding cpp files. The c++ code spits out the values into files named the same as the input files. 

* So, for processing single files:
    g++ process_$VAR.cpp -o process_$VAR.exe
where VAR is Y, X, height, etc. Then it gets run as:
    ./process_$VAR.exe $IFILE > $OFILE
where IFILE is '../../../../N19/001' and OFILE is '../../../files/$YEAR/001.txt'.

* For processing all the files (for example, Y):
    'sh process_Y.sh'

Then run through the steps listed in the first part.

**UPDATE**
24 June 2015: Committed 'run.sh', which is the control center for what variables you want to process the data with.
It includes the latitude, longitude, time, geophysical parameter, and whether you want rainy or clear-sky conditions.

This is a safeguard for getting the data confused during analysis.

### Contribution guidelines ###

* Code review

It would be helpful to make these processes more stream-lined and not so disjointed. It's really a problem with time.
I would like to develop a way to combine all the steps through a user-friendly screen so that I would be able to
switch which variables to look at, which latitude/longitude and time frame to analyze, which zenith angle tolerance is
set, etc. Also, I'd especially like to not have to run programs disjointedly and be able to call all the programs
sequentially. This would make it much more user-friendly in the future.

* Writing tests

It would be nice to develop routines that test EVERY function, but that is not the case at the moment. I have built
routines that test major vector/matrix operations and mathematical & statistical routines. These tests reside in the
'Testing' class in 'operations.h'.

* Other guidelines

N/A

### Who do I talk to? ###

For any questions contact me:
Jonathan.A.Christophersen@jpl.nasa.gov

-OR-

jchristo06@gmail.com