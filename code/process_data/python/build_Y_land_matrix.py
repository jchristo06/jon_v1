#!/Users/jchristo/anaconda/bin/python
#
#		build_Y_matrix.py
#	Author: Jonathan Christophersen
#	Data: 04 June 2015
#
#	This python script is responsible for creating a database of brightness temperatures and radar echoes from the intersection data 
#	of AMSU-B radiometer and TRMM radar. The data was processed and put into files by process_Y.cpp. 
#	This script takes the data within those files and puts it together into a matrix.
#
#

# import modules
import matplotlib.pyplot as plt
import numpy as np
import os
import glob

fSize = 5 # How long the array should be - number of rows

# Build strings
# Input
ending = ".ascii"
filePath = "../../../files/"
# filePath_python = "/Users/jonathanchristophersen/Research/AH5017/jon_V1/code/process_data/python/"
filePath_python = "/Users/jchristo/AH5017/jon_V1/code/process_data/python"
rainFilePath = str(filePath + "Y_land/rain/")
iceFilePath = str(filePath + "Y_land/ice/")
mixFilePath = str(filePath + "Y_land/mix/")
clearFilePath = str(filePath + "Y_land/clear/")

# Output
outputPath = str(filePath + "data/Y_land_mat/")
outputName_rain = "Y_land_rain_mat.txt"
outputName_ice = "Y_land_ice_mat.txt"
outputName_mix = "Y_land_mix_mat.txt"
outputName_clear = "Y_land_clear_mat.txt"
matFileName_rain = str(outputPath + outputName_rain)
matFileName_ice = str(outputPath + outputName_ice)
matFileName_mix = str(outputPath + outputName_mix)
matFileName_clear = str(outputPath + outputName_clear)

# open output file 
# Build directory if non-existant
# ../../../files/X_mat/
dir = os.path.dirname(matFileName_rain)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_rain, 'w+')

# Go back to the python directory
os.chdir(filePath_python)

# Go back to the python directory
os.chdir(filePath_python)

dir = os.path.dirname(matFileName_ice)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_rain, 'w+')

# Go back to the python directory
os.chdir(filePath_python)

dir = os.path.dirname(matFileName_mix)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_rain, 'w+')

# Go back to the python directory
os.chdir(filePath_python)


dir = os.path.dirname(matFileName_clear)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName_clear, 'w+')

#-------------------------------- RAIN --------------------------------#
# Change directory to the rain file folder
# and build the matrix for rainy cases
os.chdir(rainFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the rainy cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		rainMat = f

	if i > 0:
		rainMat = np.concatenate((rainMat, f))

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

#-------------------------------- ICE --------------------------------#
# Change directory to the rain file folder
# and build the matrix for rainy cases
os.chdir(iceFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the icy cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		iceMat = f

	if i > 0:
		iceMat = np.concatenate((iceMat, f))

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

#-------------------------------- MIX --------------------------------#
# Change directory to the rain file folder
# and build the matrix for rainy cases
os.chdir(mixFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the mixed cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		mixMat = f

	if i > 0:
		mixMat = np.concatenate((mixMat, f))

# Go back to the python directory - This is a hack, but I made filePath_2009 and 
# filePath_2010 relative to the python working directory!
os.chdir(filePath_python)

#-------------------------------- CLEAR --------------------------------#
# Change directory to the clear file folder
# and build the matrix for rainy cases
os.chdir(clearFilePath)
names = []
for file in glob.glob("*.ascii"):
	a = file
	names.append(a)

num = np.arange(fSize)
nameSize = len(names)
files = [None] * nameSize
print "Creating matrix for the clear cases ......... ", "\n"
for i in range(0, nameSize):
	data1 = str(filePath + names[i])
	files[i] = np.genfromtxt(names[i], dtype=None, usecols=num)
	f = np.array(files[i])

	if f.size == fSize:
		f = f.reshape(1, fSize)

	if i == 0:
		clearMat = f

	if i > 0:
		clearMat = np.concatenate((clearMat, f))

rainMat = rainMat.T
iceMat = iceMat.T
mixMat = mixMat.T
clearMat = clearMat.T

# # Go back to the python directory
os.chdir(filePath_python)

print "The shape of the matrices: ", "\n"
print "The rain matrix has shape ", rainMat.shape, "\n"
print "The ice matrix has shape ", iceMat.shape, "\n"
print "The mixed matrix has shape ", mixMat.shape, "\n"
print "The clear matrix has shape ", clearMat.shape, "\n"

print "Outputting the matrices to ", outputPath, "\n"
np.savetxt(matFileName_rain, rainMat)
np.savetxt(matFileName_ice, iceMat)
np.savetxt(matFileName_mix, mixMat)
np.savetxt(matFileName_clear, clearMat)
