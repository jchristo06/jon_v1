import matplotlib.pyplot as plt
import numpy as np
from MyFunc import PigeonHole

s = []
I = []
mu = []
sigma = []


sigma.append(1)
mu = np.array([0, 0])
lambdas = np.array([1, 1])

s1 = np.random.normal(mu[0], sigma[0], 1000)
s2 = np.random.normal(mu[0], sigma[0], 1000)
s.append(s1)
s.append(s2)
s = np.array(s)
# s = [1,2,3,4,5,6,7,8,9,10,2,3,4,5,2,2,5,5,5,5,5,4,3,2] # For testing

# How many bins
N1 = 100

pigeon = PigeonHole()
E = pigeon.createExtent(3.0, lambdas, mu, N1)
B = pigeon.createSpace(E)

N = s.shape

for j in range(0, N[1]):
	X = [ s[0, j], s[1, j] ]
	I.append(pigeon.getIndex(X, B))
	X = []

B = np.array(B)
I = np.array(I)

axis1 = B[0]
axis2 = B[1]
axis1 = np.array(axis1[0])
axis2 = np.array(axis2[0])

count = np.zeros((axis1.size, axis2.size))

# For 2-Dimensional case
for i in range(0, len(I)):
	a = I[i]
	count[ int(a[0] - 1), int(a[1] - 1) ] += 1
	# print count[ int(a[0] - 1), int(a[1] - 1) ]

# Hex Bins -> Didn't work
# plt.hexbin(axis1, axis2, C=count)
# plt.axis([np.amin(axis1), np.amax(axis1), np.amin(axis2), np.amax(axis2)])
# plt.savefig("./figs/hexBin_gaussian.pdf")

# Heat map
xticks = axis1[:]
yticks = axis2[:]
fig, ax = plt.subplots()
ax.pcolormesh(xticks, yticks, count[:-1], cmap=plt.cm.bone_r, edgecolors='w', vmin=np.amin(count), vmax=np.amax(count))
plt.axis([ np.amin(xticks), np.amax(xticks), np.amin(yticks), np.amax(yticks) ])
plt.xticks(xticks)
plt.yticks(yticks)
ax.xaxis.tick_top()
ax.yaxis.tick_left()
plt.xlabel("Bins for PC1")
plt.ylabel("Bins for PC2")
plt.savefig('./figs/heatMap_gaussian_histogram.eps')
plt.close()


# For 1-Dimensional case
# for i in range(0, I.size):
# 	count[int(I[i] - 1)] += 1
# 	print s[i], '  ', '   ', I[i], '   ', count[int(I[i] - 1)]
	# print i, '   ', s[i], '  ', '   ', I[i], '   ', count[int(I[i] - 1)]


# plt.bar(B.T[:-1], count[:-1], width=0.01)
# plt.savefig("./figs/gaussian_test.pdf")
# plt.close()






