#!/Users/jchristo/anaconda/bin/python
#
#		test_matrix_calcs.py
#	Author: Jonathan Christophersen
#	Date: 10 June 2015
#
#	This program creates a matrix and outputs to the appropriate directories in order to test the algorithms in the
#	'../../calculations/' directory. These algorithms include the calculation of the covariance matrices along with 
#	the eigenvalues, eigenfunctions, and singular value decomposition portion of the code.
#
#

import numpy as np
import os

# Output
filePath = "../../../files/"
outputPath = str(filePath + "data/test/")
outputName = "test_matrix.txt"
matFileName = str(outputPath + outputName)

# open output file 
# Build directory if non-existant
# ../../../files/test/
dir = os.path.dirname(matFileName)
if not os.path.exists(dir):
	print "Creating directory path ", dir, "\n"
	os.makedirs(dir)
open(matFileName, 'w+')

# Build matrix
# A = np.matrix('21.32 322.1 3.3326 45.65 -433.3 4232.32; -22.11 2.11 78.97 -0.322 3432.321 33.21; 3244.3 332.3 -33.22 98.123 32.37 78.76; 3242.124 4355.64 3426.87 -322.34 4325.64 -765.8; 423.43 -654.7 -56444.65 -654464.753 1234.7 54432.8')
# A = np.matrix('21.1 212.45 22.3 3.2; 532.321 343.4 48.9 78.2; 3123423.4 3232.67 76.5 34.3; 654.3 543.7 654.3 5432.3')
A = np.matrix('1 2 0; -1 1 1; 1 2 3')
s = A.shape
a = 0.0*np.zeros(s[0])
v = 0.0*np.zeros(s[0])
a2 = 0.0
v2 = 0.0
B = 0.0*np.array(A)
C = 0.0*np.array(A)

print "This matrix has dimensions: "
print "(", s[0], ", ", s[1], ")"
print 
print " Input Matrix: "
print A
print "----------------------------------------------------- Option 1: -----------------------------------------------------"
print

for i in range(0, s[0]):
	for j in range(0, s[1]):
		a[i] = a[i] + A[i, j] / s[1]

for i in range(0, s[0]):
	for j in range(0, s[1]):
		v[i] = v[i] + ((A[i, j] - a[i])**2.0) / s[1]

for i in range(0, s[0]):
	for j in range(0, s[1]):
		B[i, j] = (A[i, j] - a[i]) / np.sqrt(v[i])

vec_dot = np.dot(B, B.T)

print 
print " Average: "
print np.average(A, axis=1), "    ", a
print 
print " Standard Deviation: "
print np.std(A, axis=1), "    ", np.sqrt(v)
print 
print " Normalized Matrix: "
print B
print 
print " Transpose of Normalized Matrix: "
print B.T
print 
print " Dot product of B and B': "
print vec_dot
print 
print " Normalized Covariance Matrix: "
print vec_dot/(s[1] - 1)
print 

print "----------------------------------------------------- Option 2: -----------------------------------------------------"
print
for i in range(0, s[0]):
	for j in range(0, s[1]):
		a2 = a2 + A[i, j] / A.size

for i in range(0, s[0]):
	for j in range(0, s[1]):
		v2 = v2 + ((A[i, j] - a2)**2.0) / A.size

for i in range(0, s[0]):
	for j in range(0, s[1]):
		C[i, j] = (A[i, j] - a[i]) / np.sqrt(v2)

vec_dot2 = np.dot(C, C.T)
print " Average: "
print a2
print 
print " Variance: "
print v2
print 
print " Standard Deviation: "
print np.sqrt(v2)
print 
print " Normalized Matrix: "
print C
print 
print " Transpose of Normalized Matrix: "
print C.T
print 
print " Dot product of C and C': "
print vec_dot2
print 
print " Normalized Covariance Matrix: "
print vec_dot2/(s[1] - 1)
print 
print np.cov(A)




































np.savetxt(matFileName, A)
