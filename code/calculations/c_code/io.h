#ifndef IO_H
#define IO_H

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

class Input
{
	public:
		std::vector< std::vector<double> > read2DVector( std::string fp ); // Read in a 2D matrix and return 2D vector
};

class Output
{
	public:
		void write2DVector( std::string fp, std::vector< std::vector<double> > v );	// Output 2D vector as ASCII
		void write1DVector( std::string fp, std::vector<double> v); // Output 1D vector as ASCII
};

#endif