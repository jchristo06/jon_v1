#ifndef GLOBAL_H
#define GLOBAL_H

#include <string>

extern std::string data;

/*------------------------------------------- For Input -------------------------------------------*/
// Path to x and y matrix files
extern std::string x_var;
extern std::string y_var;
extern std::string h_var;
extern std::string x_land_var;
extern std::string y_land_var;
extern std::string test_path;
extern std::string total_tb_var;
extern std::string cwm_var;

// FILE NAMES:
// Rain
extern std::string x_rain;
extern std::string y_rain;
extern std::string h_rain;

// Clear
extern std::string x_clear;
extern std::string y_clear;

// Ice
extern std::string x_ice;
extern std::string y_ice;
extern std::string h_ice;

// Mix
extern std::string x_mix;
extern std::string y_mix;
extern std::string h_mix;

// Total Brightness temps
extern std::string tb_rain;
extern std::string tb_clear;

// Land rain
extern std::string xL_rain;
extern std::string yL_rain;

// Land clear
extern std::string xL_clear;
extern std::string yL_clear;

// Land ice
extern std::string xL_ice;
extern std::string yL_ice;

// Land mix
extern std::string xL_mix;
extern std::string yL_mix;

// Cloud water mass
extern std::string cwm_x;
extern std::string cwm_y;

// Test
extern std::string test_fn;

// GLUE STRINGS TOGETHER:
// Rain
extern std::string xr;		// x rain
extern std::string yr;		// y rain
extern std::string hr;		// h rain

// Clear
extern std::string xc;		// x clear
extern std::string yc;		// y clear

// Ice
extern std::string xi;		// x ice
extern std::string yi;		// y ice
extern std::string hi;		// h ice

// Mix
extern std::string xm;		// x mix
extern std::string ym;		// y mix
extern std::string hm;		// h mix

// Total brightness temps
extern std::string tbr; 	// Tb rainy
extern std::string tbc; 	// Tb clear

// Land rain
extern std::string xLr;	 	// x land rain
extern std::string yLr;	 	// y land rain

// Land clear
extern std::string xLc;	 	// x land clear
extern std::string yLc;	 	// y land clear

// Land ice
extern std::string xLi;	 	// x land ice
extern std::string yLi;	 	// y land ice

// Land mix
extern std::string xLm;	 	// x land mix
extern std::string yLm;	 	// y land mix

// CWM
extern std::string xCwm;
extern std::string yCwm;

// Test
extern std::string test;	// test

/*------------------------------------------- For Output -------------------------------------------*/
extern std::string c_output;

// Rain
extern std::string cxx_r;
extern std::string cyy_r;
extern std::string cxy_r;
extern std::string chh_r;
extern std::string txx_r;
extern std::string txy_r;
extern std::string NormX_r;
extern std::string NormY_r;
extern std::string NormH_r;

// Clear
extern std::string cxx_c;
extern std::string cyy_c;
extern std::string cxy_c;
extern std::string txx_c;
extern std::string txy_c;
extern std::string NormX_c;
extern std::string NormY_c;

// Ice
extern std::string cxx_i;
extern std::string cyy_i;
extern std::string cxy_i;
extern std::string chh_i;
extern std::string txx_i;
extern std::string txy_i;
extern std::string NormX_i;
extern std::string NormY_i;
extern std::string NormH_i;

// Mix
extern std::string cxx_m;
extern std::string cyy_m;
extern std::string cxy_m;
extern std::string chh_m;
extern std::string txx_m;
extern std::string txy_m;
extern std::string NormX_m;
extern std::string NormY_m;
extern std::string NormH_m;

// Rain over the land
extern std::string cxx_l_r;
extern std::string cyy_l_r;
extern std::string cxy_l_r;
extern std::string chh_l_r;
extern std::string txx_l_r;
extern std::string txy_l_r;
extern std::string NormX_l_r;
extern std::string NormY_l_r;
extern std::string NormH_l_r;

// Clear over land
extern std::string cxx_l_c;
extern std::string cyy_l_c;
extern std::string cxy_l_c;
extern std::string txx_l_c;
extern std::string txy_l_c;
extern std::string NormX_l_c;
extern std::string NormY_l_c;

// Ice over land
extern std::string cxx_l_i;
extern std::string cyy_l_i;
extern std::string cxy_l_i;
extern std::string chh_l_i;
extern std::string txx_l_i;
extern std::string txy_l_i;
extern std::string NormX_l_i;
extern std::string NormY_l_i;
extern std::string NormH_l_i;

// Mix over land
extern std::string cxx_l_m;
extern std::string cyy_l_m;
extern std::string cxy_l_m;
extern std::string chh_l_m;
extern std::string txx_l_m;
extern std::string txy_l_m;
extern std::string NormX_l_m;
extern std::string NormY_l_m;
extern std::string NormH_l_m;

// Total Tb
extern std::string NormTb_r;
extern std::string Tb_r;

// CWM
extern std::string NormX_cwm;
extern std::string NormY_cwm;
extern std::string cxx_cwm;
extern std::string cyy_cwm;
extern std::string cxy_cwm;



#endif