
#include "stats.h"

/*																Statistics Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Get the statistics - we are opting for Step 2(a), i.e. the std at each level.
std::vector< std::vector<double> > Stats::Option1( std::vector< std::vector<double> > v )
{
	// V1:
	std::vector<double> average = Average1(v);
	std::vector<double> variance = Variance1(v, average);
	std::vector<double> std = StdDev1(variance);
	std::vector< std::vector<double> > var_prime = Normalize1(v, average, std);

	return var_prime;
}

// Get the statistics - we are opting for Step 2(b), i.e. the overall std.
std::vector< std::vector<double> > Stats::Option2( std::vector< std::vector<double> > v )
{
	double average = Average_All(v);
	std::vector<double> average2 = Average1(v);
	double variance = Variance_All(v, average);
	double std = StdDev_All(variance);
	std::vector< std::vector<double> > var_prime = Normalize_All(v, average2, std);

	return var_prime;
}

// Read in the NORMALIZED matrices.
std::vector< std::vector<double> > Stats::CovarianceMatrix( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 )
{
	VecOps ops;

	// Transpose second vector and calculate the product
	std::vector< std::vector<double> > v2_T = ops.MatrixTranspose(v2);
	std::vector< std::vector<double> > A = ops.DotProduct(v1, v2_T);
	
	int rows = A.size();
	int cols = A[0].size();
	std::vector< std::vector<double> > cov(rows, std::vector<double> (cols));

	if (rows == cols)
	{
		std::cout << "\n";
		std::cout << "Calculating the Normalized Auto-Covariance Matrix ...... " << "\n";
		std::cout << "Matrix is square." << "\n";
		std::cout << "Matrix has dimensions (" << rows << ", " << cols << ")" << "\n";
		std::cout << "\n";
		std::cout << "\n";
		for (int i = 0; i < A.size(); i++)
		{
			for (int j = 0; j < A[i].size(); j++)
			{
				cov[i][j] = A[i][j] / (v1[0].size() - 1);
			}
		}
	}
	else
	{
		std::cout << "\n";
		std::cout << "Calculating the Normalized Cross-Covariance Matrix ...... " << "\n";
		std::cout << "Matrix is NOT square." << "\n";
		std::cout << "Matrix has dimensions (" << rows << ", " << cols << ")" << "\n";
		std::cout << "\n";
		std::cout << "\n";
		for (int i = 0; i < A.size(); i++)
		{
			for (int j = 0; j < A[i].size(); j++)
			{
				cov[i][j] = A[i][j] / (v1[0].size() - 1);
			}
		}
		// exit(-1);
	}

	return cov;
}

/*------------------ OPTION 1 for normalization: ------------------*/
// Average
std::vector<double> Stats::Average1( std::vector< std::vector<double> > v )
{
	int i, j = 0;
	std::vector<double> avg;
	double a = 0.0;
	int count = 0;

	for (i = 0; i < v.size(); i++)
	{
		a = 0;
		for (j = 0; j < v[i].size(); j++)
		{
			a += v[i][j];
			count++;
		}
		
		if (isnan(a/count) == 1)
		{
			a = 0;
			count = 1;
		}

		avg.push_back(a/count);
		count = 0;
	}

	return avg;
}


// Variance
std::vector<double> Stats::Variance1( std::vector< std::vector<double> > v , std::vector<double> avg )
{
	int i, j = 0;
	double diff = 0.0;
	double var_i = 0.0;
	double var = 0.0;
	std::vector<double> variance;
	int count = 0;

	for (i = 0; i < v.size(); i++)
	{
		var = 0;
		for (j = 0; j < v[i].size(); j++)
		{
			diff = v[i][j] - avg[i];
			var_i = pow(diff, 2);
			var += var_i;
			count++;
		}
		
		if (isnan(var/count) == 1)
		{
			var = 0;
			count = 1;
		}

		variance.push_back(var/(count));
		count = 0;
	}

	return variance;
}

std::vector<double> Stats::StdDev1( std::vector<double> variance )
{
	std::vector<double> std;

	for (int i = 0; i < variance.size(); i++)
	{
		std.push_back(sqrt(variance[i]));
	}

	return std;
}

std::vector< std::vector<double> > Stats::Normalize1( std::vector< std::vector<double> > v, std::vector<double> avg, std::vector<double> std )
{
	int sz = v.size();
	int sz2 = v[0].size();
	// Set size of the "normalized" vector
	std::vector< std::vector<double> > normalized(v.size(), std::vector<double> (v[0].size()));

	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j< v[i].size(); j++)
		{
			if (std[i] != 0.0)
			{
				normalized[i][j] = (v[i][j] - avg[i])/(std[i]);
			}
			else
			{
				std::cout << " Division by zero in Stats::Normalize() .... " << "\n";
				normalized[i][j] = 0.0;
				exit(-1);
			}
		}
	}

	return normalized;
}

/*------------------ OPTION 2 for normalization: ------------------*/
// Average over all levels
double Stats::Average_All( std::vector< std::vector<double> > v )
{
	int i, j = 0;
	int count = 0;
	double a = 0.0;

	for (i = 0; i < v.size(); i++)
	{
		for (j = 0; j < v[i].size(); j++)
		{
			a += v[i][j];
			count++;
		}
	}

	return a/count;
}

// Variance
// Use the average quantity over all levels to calculate the overall variance.
double Stats::Variance_All( std::vector< std::vector<double> > v , double avg )
{
	int i, j = 0.0;
	double diff = 0.0;
	double var_i = 0.0;
	double var = 0.0;
	int count = 0;

	for (i = 0; i < v.size(); i++)
	{
		for (j = 0; j < v[i].size(); j++)
		{
			diff = v[i][j] - avg;
			var_i = pow(diff, 2.0);
			var += var_i;
			count++;
		}
	}

	return var/count;
}

double Stats::StdDev_All( double variance )
{
	double std = sqrt(variance);

	return std;
}

std::vector< std::vector<double> > Stats::Normalize_All( std::vector< std::vector<double> > v, std::vector<double> avg, double std )
{
	int sz = v.size();
	int sz2 = v[0].size();
	// Set size of the "normalized" vector
	std::vector< std::vector<double> > normalized(v.size(), std::vector<double> (v[0].size()));
	
	for (int i = 0; i < sz; i++)
	{
		for (int j = 0; j < sz2; j++)
		{
			normalized[i][j] = (v[i][j] - avg[i])/std;
		}
	}

	return normalized;
}






/*------------------------------------------------------ OLD CODE ------------------------------------------------------*/

// Skewness
double Stats::Skewness( std::vector< double > ts, double avg, double var )
{
	int i = 0;
	double diff = 0.0;
	double diff2 = 0.0;
	double skew = 0.0;

	for (i = 0; i < ts.size(); i++)
	{
		if (isnan(ts[i]) == 0)
		{
			diff = (ts[i] - avg);
			diff2 = pow(diff, 3);
			skew = skew + diff2;
		}
	}

	if (var != 0)
	{
		skew = skew/ts.size();
		skew = skew/(sqrt(var)*var);
	}
	else
	{
		skew = 0;
	}

	return skew;
}

// Kurtosis
double Stats::Kurtosis( std::vector< double > ts, double avg, double var )
{
	int i = 0;
	double diff = 0.0;
	double diff2 = 0.0;
	double kurt = 0.0;

	for (i = 0; i < ts.size(); i++)
	{
		if (isnan(ts[i]) == 0)
		{
			diff = (ts[i] - avg);
			diff2 = pow(diff, 4);
			kurt = kurt + diff2;
		}
		else 
		{
			kurt = 0;
		}
	}

	if (var != 0)
	{
		kurt = kurt/ts.size();
		kurt = kurt/(var*var);
		kurt = kurt - 3;
	}
	else
	{
		kurt = 0;
	}

	return kurt;
}

std::vector< double > Stats::Anom( std::vector< double > ts, double avg, double var )
{
	int i = 0;
	double diff = 0.0;
	double dummy = 0.0;
	std::vector<double> anom;
	std::vector<double> anomDummy;

	for (i = 0; i < ts.size(); i++)
	{
		if (isnan(ts[i]) == 0)
		{
			diff = (ts[i] - avg);
			anom.push_back(diff);
		}
	}

	return anom;
}

// This subroutine is to calcuate the median of the data
// 12-22-14
double Stats::Median( std::vector< double > A )
{
	double Med = 0.0;
	Data dat;
	std::vector<double> v;

	v = dat.QuickSort(A, 0, A.size());

	if (v.size() % 2 != 0)
	{
		Med = A[A.size()/2];
	}
	else
	{
		Med = (A[A.size()/2] + A[A.size()/2 + 1]) / 2.0;
	}

	return Med;
}

// This subroutine is to calcuate the z-score of the data
// 12-22-14
std::vector< double > Stats::Biweight( std::vector< double > A )
{
	int i = 0;
	float c = 7.5; // "censor" in the weighting function. Will need testing.
	double diff = 0.0; // dummy variable
	double dum = 0.0; // dummy variable
	double biweightMean = 0.0; // Biweight mean
	double biweightSTD = 0.0; // Biweight standard deviation
	std::vector<double> v; // v variable
	std::vector<double> toterror; // total error
	std::vector<double> w; // weighting function
	std::vector<double> zscore; // z-score

	for (i = 0; i < A.size(); i++)
	{
		v.push_back(fabs(A[i]));
	}

	double Med = Median(A); // Get median of data
	double Mad = Median(v); // Get mean absolute deviation

	// Get total error
	for (i = 0; i < A.size(); i++)
	{
		diff = A[i] - Med;
		toterror.push_back(diff);
	}

	// derive weight w=(Xi - M)/(c * MAD)
	for (i = 0; i < A.size(); i++)
	{
		w.push_back(toterror[i]/(c * Mad));
		if (w[i] >= 1.0)
		{
			w[i] = 1.0;
		}
	}

	// derive biweight mean Xbi=M + nmt/dmt
	double nmt = 0.0;
	double dmt = 0.0;

	for (i = 0; i < A.size(); i++)
	{
		nmt = nmt + (toterror[i] * pow( (1 - pow( w[i], 2 ) ), 2 ));
		dmt = dmt + (pow( (1 - pow( w[i], 2 ) ), 2 ));
	}
	biweightMean = Med + nmt / dmt;

	// derive biweight standard deviation
	double up = 0.0;
	double down = 0.0;

	for (i = 0; i < A.size(); i++)
	{
		up = up + (pow( toterror[i], 2 ) * pow(1 - pow( w[i], 2 ), 4));
		down = down + (1 - pow( w[i], 2 ) * (1 - 5 * pow( w[i], 2 )));
	}

	up = sqrt(A.size() * up);
	down = fabs(down);
	biweightSTD = up / down;

	// derive the z-score
	for (i = 0; i < A.size(); i++)
	{
		dum = (A[i] - biweightMean) / biweightSTD;
		zscore.push_back(dum);
	}

	return zscore;
}


std::vector< double > Stats::AutoCov( std::vector< double > v, int lag )
{
	int N = v.size();
	int i, j;
	double dat = 0.0;
	std::vector<double> autocov;

	for (i = 1; i < lag; i++)
	{
		for (j = lag+1; j < N - lag; j++)
		{
			dat += (v[j] * v[j + i])/N;
		}
		autocov.push_back(dat);
	}

	return autocov;
}
























