/*
						Operations.h
				Author: Jonathan Christophersen
				Date: 23 November 2014

*/

#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include "global.h"
#include "stats.h"
#include "io.h"


/* This class handles the data management and filtering. */
class Data
{
public:
	std::vector< double > lowPass( std::vector< double > v ); 	// Low pass filter to suppress spurious spikes - very simple filter
	std::vector< double > Smoothing( std::vector< double > v ); // Smoothing algorithm
	int Partition( std::vector< double >& A, int p, int q ); // Gets data ready for QuickSort() - returns where to partition data
	std::vector< double > QuickSort( std::vector< double >& A, int p, int q ); // Arranges data from low to high - used for calculation of median
	std::vector< double > Resize1D( std::vector< double > v, int min, int max ); // Resize vector
	std::vector< std::vector<double> > Resize2D( std::vector< std::vector<double> > v, int row_min, int row_max, int col_min, int col_max ); //resizes the matrix
																																			// with given bounds.
};

/* This class handles operations performed on the matrices/vectors */
class VecOps
{
public:
	std::vector< std::vector<double> > MatrixTranspose( std::vector< std::vector<double> > v1 );
	std::vector< std::vector<double> > VectorTranspose( std::vector<double> v );
	std::vector< std::vector<double> > DotProduct( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 ); // Take two matrices and multiply them.
	std::vector<double> VectorSubtract( std::vector<double> v1, std::vector<double> v2 ); // Subtract two vectors - 1D only!!
	std::vector< std::vector<double> > MatrixAddition( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 ); // Add two matrices together, element by element
	std::vector< std::vector<double> > MatrixNonNegative( std::vector< std::vector<double> > v);
	std::vector< std::vector<double> > ScalarProduct( double a, std::vector< std::vector<double> > v );
};

/* This class is designed to calculate the linear discriminant of the brightness temperatures for clear-sky vs. rainy conditions */
class LinearDiscriminant
{
public:
	std::vector< std::vector<double> > LinDisc();
};

/*

Along with MatrixTranspose(), these member functions are used to calculate MatrixInverse().
We are finding the inverse of matrix A:

					A^(-1) = (1/det(A)) * adj(A)

The original source code is written by Paul Bourke and can be found at:

		www.cs.rochester.edu/~brown/Crypto/assts/projects/adj.html

*/
class MatrixInverse
{
public:
	std::vector< std::vector<double> > matrixInverse( std::vector< std::vector<double> > v );
	double Determinant( std::vector< std::vector<double> > v, int N );
	std::vector< std::vector<double> > CoFactor( std::vector< std::vector<double> > v, int N ); // Transpose of the adjoint matrix
};

#endif
