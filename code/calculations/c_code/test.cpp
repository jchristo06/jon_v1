
#include "test.h"

/*																	Testing Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void Testing::ShowMatrix( std::vector< std::vector<double> > v )
{
	std::cout << " Input Matrix: " << "\n";
	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j < v[i].size(); j++)
		{
			std::cout << v[i][j] << " ";
		}
		std::cout << "\n";
	}
}


std::vector<double> Testing::TestAvg( std::vector< std::vector<double> > v )
{
	Stats a;

	std::vector<double> avg = a.Average1(v);

	std::cout << "\n";
	std::cout << " Average: " << "\n";
	for (int i = 0; i < avg.size(); i++)
	{
		std::cout << avg[i] << "\n";
	}

	return avg;

}

std::vector<double> Testing::TestVar( std::vector< std::vector<double> > v, std::vector<double> avg )
{
	Stats a;

	std::vector<double> var = a.Variance1(v, avg);
	
	std::cout << "\n";
	std::cout << " Variance: " << "\n";
	for (int i = 0; i < var.size(); i++)
	{
		std::cout << var[i] << "\n";
	}

	return var;
}

std::vector<double> Testing::TestStd( std::vector<double> var )
{
	Stats a;

	std::vector<double> std = a.StdDev1(var);
	
	std::cout << "\n";
	std::cout << " Standard Deviation: " << "\n";
	for (int i = 0; i < var.size(); i++)
	{
		std::cout << std[i] << "\n";
	}

	return std;
}

std::vector< std::vector<double> > Testing::TestNormalize( std::vector< std::vector<double> > v, std::vector<double> avg, std::vector<double> std )
{
	Stats a;

	std::vector< std::vector<double> > norm = a.Normalize1(v, avg, std);

	std::cout << "\n";
	std::cout << " Normalized Matrix: " << "\n";
	for (int i = 0; i < norm.size(); i++)
	{
		for (int j = 0; j < norm[0].size(); j++)
			std::cout << norm[i][j] << "    ";
		std::cout << "\n";
	}

	return norm;
}

std::vector< std::vector<double> > Testing::TestMatrixTranspose( std::vector< std::vector<double> > v )
{
	VecOps a;

	std::vector< std::vector<double> > trans = a.MatrixTranspose(v);

	std::cout << "\n";
	std::cout << " Transposed Matrix: " << "\n";
	for (int i = 0; i < trans.size(); i++)
	{
		for (int j = 0; j < trans[0].size(); j++)
			std::cout << trans[i][j] << "    ";
		std::cout << "\n";
	}

	return trans;
}

std::vector< std::vector<double> > Testing::TestDotProduct( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 )
{
	VecOps a;

	std::vector< std::vector<double> > prod = a.DotProduct(v1, v2);

	std::cout << "\n";
	std::cout << " Matrix Product: " << "\n";
	for (int i = 0; i < prod.size(); i++)
	{
		for (int j = 0; j < prod[0].size(); j++)
			std::cout << prod[i][j] << " ";
		std::cout << "\n";
	}

	return prod;
}

void Testing::TestCovariance( std::vector< std::vector<double> > v1, std::vector< std::vector<double> > v2 )
{
	Stats a;

	std::vector< std::vector<double> > v_op1 = a.Option2(v1);
	std::vector< std::vector<double> > v_op2 = a.Option2(v1);

	std::vector< std::vector<double> > cov = a.CovarianceMatrix(v_op1, v_op2);

	std::cout << "\n";
	std::cout << " Normalized Covariance Matrix: " << "\n";
	for (int i = 0; i < cov.size(); i++)
	{
		for (int j = 0; j < cov[0].size(); j++)
		{
			std::cout << cov[i][j] << "    ";
		}
		std::cout << "\n";
	}
}

double Testing::TestAvg_All( std::vector< std::vector<double> > v )
{
	Stats a;

	double avg = a.Average_All(v);

	std::cout << "\n";
	std::cout << " Average: " << "\n";
	std::cout << avg << "\n";

	return avg;
}

double Testing::TestVar_All( std::vector< std::vector<double> > v , double avg )
{
	Stats a;

	double var = a.Variance_All(v, avg);
	
	std::cout << "\n";
	std::cout << " Variance: " << "\n";
	std::cout << var << "\n";

	return var;
}

double Testing::TestStd_All( double variance )
{
	std::cout << "\n";
	std::cout << " Standard Deviation: " << "\n";
	std::cout << sqrt(variance) << "\n";

	return sqrt(variance);
}

std::vector< std::vector<double> > Testing::TestNormalize_All( std::vector< std::vector<double> > v, std::vector<double> avg, double std )
{
	Stats a;

	std::vector< std::vector<double> > norm = a.Normalize_All(v, avg, std);

	std::cout << "\n";
	std::cout << " Normalized Matrix: " << "\n";
	for (int i = 0; i < norm.size(); i++)
	{
		for (int j = 0; j < norm[0].size(); j++)
			std::cout << norm[i][j] << "    ";
		std::cout << "\n";
	}

	return norm;
}


void Testing::TestMatrixNonNegative( std::vector< std::vector<double> > v )
{
	VecOps a;

	std::vector< std::vector<double> > newVec = a.MatrixNonNegative(v);

	for (int i = 0; i < newVec.size(); i++)
	{
		for (int j = 0; j < newVec[0].size(); j++)
			std::cout << newVec[i][j] << "    ";
		std::cout << "\n";
	}
}


/*															Testing Inverse Class																*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void TestInverse::TestDeterminant( std::vector<std::vector<double> > v, int N )
{
	MatrixInverse matInv;

	double det = matInv.Determinant(v, N);

	std::cout << "\n";
	std::cout << "The determinant is ";
	std::cout << det << "\n";
}


void TestInverse::TestCofactor( std::vector< std::vector<double> > v, int N)
{
	MatrixInverse matInv;

	std::vector< std::vector<double> > cofactor = matInv.CoFactor(v, N);

	for (int i = 0; i < cofactor.size(); i++)
	{
		for (int j = 0; j < cofactor[0].size(); j++)
		{
			std::cout << cofactor[i][j] << "   ";
		}
		std::cout << "\n";
	}
}































