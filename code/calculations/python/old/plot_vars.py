#!/Users/jchristo/anaconda/bin/python
#
#		plot_vars.py
#	Author: Jonathan Christophersen
#	Data: 04 June 2015
#
#	This program is responsible for reading and plotting the matrices from ../../../files/data/Y_var and ../../../files/data/X_var.
# 	These paths contain the data for both clear sky readings and rain events.
#
#	REMEMBER: 
#	Y is the matrix that contains all data from radiometer and radar. It is the raw observation.
#	X is the matrix that contains the derived variables, such as ice water content, liquid water content, mixed water content, etc.
#
#



# import modules
import matplotlib.pyplot as plt
import pylab as P
import numpy as np
import os
import glob
import csv

# TeX fonts
# plt.rc('text', usetex=True)
# plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
plt.rc('axes', **{'titlesize':12})
plt.rc('legend', **{'fontsize': 11})
plt.rcParams['legend.frameon'] = 'False'

# Set path and file names:
filePath = "../../../files/data/"
outputPath = "./figs/"
scatter_name = "Tb4_vs_Tb5.pdf"

# Input PC scores 1 and 2; paths and file names
pcscore1_clear_fn = "pc_scores1_clear.txt"
pcscore2_clear_fn = "pc_scores2_clear.txt"
pcscore1_rain_fn = "pc_scores1_rain.txt"
pcscore2_rain_fn = "pc_scores2_rain.txt"
pca_path = str(filePath + "pca/")
pca1_clear = str(pca_path + pcscore1_clear_fn)
pca2_clear = str(pca_path + pcscore2_clear_fn)
pca1_rain = str(pca_path + pcscore1_rain_fn)
pca2_rain = str(pca_path + pcscore2_rain_fn)

# Input NORMALIZED data paths and file names
calcPath = "calc_output/"
yrain = "NormY_rain.txt"
yclear = "NormY_clear.txt"
xrain = "NormX_rain.txt"
xclear = "NormX_clear.txt"
normPath = str(filePath + calcPath)
total_y = str(normPath + yrain)
total_x = str(normPath + xrain)

# Input MATRIX data paths and file names
yPath = str(filePath + "Y_mat/")
xPath = str(filePath + "X_mat/")
hPath = str(filePath + "H_mat/")
tPath = str(filePath + "tots_mat/")

yRainName = "Y_rain_mat.txt"
xRainName = "X_rain_mat.txt"
hRainName = "H_rain_mat.txt"
tRainName = "tots_rain_mat.txt"
yClearName = "Y_clear_mat.txt"
xClearName = "X_clear_mat.txt"
hClearName = "H_clear_mat.txt"
tClearName = "tots_clear_mat.txt"

yRainData = str(yPath + yRainName)
xRainData = str(xPath + xRainName)
hRainData = str(hPath + hRainName)
tRainData = str(tPath + tRainName)
yClearData = str(yPath + yClearName)
xClearData = str(xPath + xClearName)
hClearData = str(hPath + hClearName)
tClearData = str(tPath + tClearName)

# Read in the matrix data
print 
print "Read in MATRIX DATA"
print 

print "Reading in ", yRainData, " ....... "
rainyY = np.genfromtxt(yRainData)

print "Reading in ", yClearData, " ....... "
clearY = np.genfromtxt(yClearData)

print "Reading in ", xRainData, " ....... "
rainyX = np.genfromtxt(xRainData)

print "Reading in ", xClearData, " ....... "
clearX = np.genfromtxt(xClearData)

# print "Reading in ", hRainData, " ....... "
# rainyH = np.genfromtxt(hRainData)

# print "Reading in ", hClearData, " ....... "
# clearH = np.genfromtxt(hClearData)

# print "Reading in ", tRainData, " ....... "
# rainyT = np.genfromtxt(tRainData)

# print "Reading in ", tClearData, " ....... "
# clearT = np.genfromtxt(tClearData)

# Read in data
print 
print "Read in PC SCORE & NORMALIZED DATA"
print 
print "Reading in ", pca1_clear, " ....... "
pcScore1_c = np.genfromtxt(pca1_clear)
print "Reading in ", pca2_clear, " ....... "
pcScore2_c = np.genfromtxt(pca2_clear)
print "Reading in ", pca1_rain, " ....... "
pcScore1_r = np.genfromtxt(pca1_rain)
print "Reading in ", pca2_rain, " ....... "
pcScore2_r = np.genfromtxt(pca2_rain)
print "Reading in ", total_y, " ....... "
Y = np.genfromtxt(total_y)
print "Reading in ", total_x, " ....... "
X = np.genfromtxt(total_x)

#---------------------------------- Structure the data ----------------------------------#
rowSizeYRain = rainyY.shape[0] # Number of rows in Y structures for rain cases
rowSizeXRain = rainyX.shape[0] # Number of rows in X structure for rain cases
# rowSizeHRain = rainyH.shape[0] # Number of rows in H structures for rain cases
# rowSizeTRain = rainyT.shape[0] # Number of rows in T structure for rain cases

colSizeYRain = rainyY.shape[1] # Number of columns in Y structures for rain cases
colSizeXRain = rainyX.shape[1] # Number of columns in X structure for rain cases
# colSizeHRain = rainyH.shape[1] # Number of columns in H structure for rain cases
# colSizeTRain = rainyT.shape[1] # Number of columns in T structure for rain cases

rowSizeYClear = clearY.shape[0] # Number of rows in Y structure for clear cases
rowSizeXClear = clearX.shape[0] # Number of rows in X structure for clear cases
# rowSizeHClear = clearH.shape[0] # Number of rows in H structure for clear cases
# rowSizeTClear = clearT.shape[0] # Number of rows in T structure for clear cases

colSizeYClear = clearY.shape[1] # Number of columns in Y structure for clear cases
colSizeXClear = clearX.shape[1] # Number of columns in X structure for clear cases
# colSizeHClear = clearH.shape[1] # Number of columns in H structure for clear cases
# colSizeTClear = clearT.shape[1] # Number of columns in T structure for clear cases

# Error checking - checking if bounds are correct
if rowSizeYRain != rowSizeYClear:
	print "Rows for Y rain are not equal to rows for Y clear .... "
if rowSizeXRain != rowSizeXClear:
	print "Rows for X rain are not equal to rows for X clear .... "
if colSizeYRain != colSizeXRain:
	print "Columns for Y rain are not equal to columns for X rain .... "
if colSizeYClear != colSizeXClear:
	print "Columns for Y clear are not equal to columns for X clear .... "

# Get brightness temperatures for RAINY conditions
Tb1_r = np.zeros(shape=(colSizeYRain, 1))
Tb2_r = np.zeros(shape=(colSizeYRain, 1))
Tb3_r = np.zeros(shape=(colSizeYRain, 1))
Tb4_r = np.zeros(shape=(colSizeYRain, 1))
Tb5_r = np.zeros(shape=(colSizeYRain, 1))

for i in range(0, colSizeYRain):
	Tb1_r[i] = rainyY[0][i]
	Tb2_r[i] = rainyY[1][i]
	Tb3_r[i] = rainyY[2][i]
	Tb4_r[i] = rainyY[3][i]
	Tb5_r[i] = rainyY[4][i]

# Get brightness temperatures for CLEAR conditions
Tb1_c = np.zeros(shape=(colSizeYClear, 1))
Tb2_c = np.zeros(shape=(colSizeYClear, 1))
Tb3_c = np.zeros(shape=(colSizeYClear, 1))
Tb4_c = np.zeros(shape=(colSizeYClear, 1))
Tb5_c = np.zeros(shape=(colSizeYClear, 1))

for i in range(0, colSizeYClear):
	Tb1_c[i] = clearY[0][i]
	Tb2_c[i] = clearY[1][i]
	Tb3_c[i] = clearY[2][i]
	Tb4_c[i] = clearY[3][i]
	Tb5_c[i] = clearY[4][i]

# Get dervied variables for rainy cases at a particular level
lev = 48 # As lev increases, the further down we are going in the column
height_from_lev = (80 - lev) * 250
var = np.zeros(shape=(colSizeYRain, 1))
for i in range(0, colSizeYRain):
	var[i] = rainyX[lev][i]


#------------------------------------------ PLOTTING SECTION ------------------------------------------#

#---------------------------------- LINE PLOTS ----------------------------------#
# height = np.arange(20, 0, -0.250)
# Tb = [1, 2, 3, 4, 5]
# labels = ['89', '150', '183.1$\pm 1.0$', '183.1$\pm 3.0$', '183.1$\pm 7.0$']

# # Plot the eigenvalues
# EigenValName = str(figPath + "EigVals.pdf")
# plt.plot(eigVal_xx, "b-o", linewidth=2, label=r"Eigenvalues for C$_{xx}$")
# plt.plot(eigVal_yy, "r-o", linewidth=2, label=r"Eigenvalues for C$_{yy}$")
# plt.plot(Sxy, "y-o", linewidth=2, label=r"Eigenvalues for C$_{xy}$")
# plt.xlim(0, 5)
# plt.ylabel(r"Eigenvalue")
# plt.title(r"Eigenvalues for Covariance Matrices C$_{xy}$, C$_{xx}$, and C$_{yy}$")
# plt.legend(loc="best")
# plt.savefig(EigenValName)
# plt.close()

# # Plot first 3 EigenVectors of Cxx and Cyy
# EigenVecName = str(figPath + "EigVecs_cxx.pdf")
# plt.plot(height, eigVec_xx[:,0], "b-o", linewidth=2, label=r"1$^{\mathrm{st}}$ Eigenvector for C$_{\mathrm{xx}}$", alpha=0.65)
# plt.plot(height, eigVec_xx[:,1], "r-o", linewidth=2, label=r"2$^{\mathrm{nd}}$ Eigenvector for C$_{\mathrm{xx}}$", alpha=0.65)
# plt.plot(height, eigVec_xx[:,2], "y-o", linewidth=2, label=r"3$^{\mathrm{rd}}$ Eigenvector for C$_{\mathrm{xx}}$", alpha=0.65)
# plt.xlabel(r"Height (km)")
# plt.ylabel(r"Eigenvector")
# plt.legend(loc="best")
# plt.title(r"First three eigenvectors for C$_{\mathrm{xx}}$")
# plt.savefig(EigenVecName)
# plt.close()

# EigenVecName2 = str(figPath + "EigVecs_cyy.pdf")
# plt.plot(Tb, eigVec_yy[:,0], "b-o", linewidth=2, label=r"1$^{\mathrm{st}}$ Eigenvector for C$_{\mathrm{yy}}$", alpha=0.65)
# plt.plot(Tb, eigVec_yy[:,1], "r-o", linewidth=2, label=r"2$^{\mathrm{nd}}$ Eigenvector for C$_{\mathrm{yy}}$", alpha=0.65)
# plt.plot(Tb, eigVec_yy[:,2], "y-o", linewidth=2, label=r"3$^{\mathrm{rd}}$ Eigenvector for C$_{\mathrm{yy}}$", alpha=0.65)
# plt.xticks(Tb, labels)
# plt.xlabel(r"Channel")
# plt.ylabel(r"Eigenvector")
# plt.xlim(0, 6)
# plt.legend(loc="best")
# plt.title(r"First three eigenvectors for C$_{\mathrm{yy}}$")
# plt.savefig(EigenVecName2)
# plt.close()

# # Plot first 3 eigenvectors of Cxy
# EigenVecName3 = str(figPath + "EigVecs_cxy_u.pdf")
# plt.plot(height, Uxy[0], "b-o", linewidth=2, label=r"1$^{\mathrm{st}}$ Eigenvector for C'$_{\mathrm{xy}}$ C$_{\mathrm{xy}}$", alpha=0.65)
# plt.plot(height, Uxy[1], "r-o", linewidth=2, label=r"2$^{\mathrm{nd}}$ Eigenvector for C'$_{\mathrm{xy}}$ C$_{\mathrm{xy}}$", alpha=0.65)
# plt.plot(height, Uxy[2], "y-o", linewidth=2, label=r"3$^{\mathrm{rd}}$ Eigenvector for C'$_{\mathrm{xy}}$ C$_{\mathrm{xy}}$", alpha=0.65)
# plt.xlabel(r"Height (km)")
# plt.ylabel(r"Eigenvector")
# plt.legend(loc="best")
# plt.title(r"First three eigenvectors for C'$_{\mathrm{xy}}$ C$_{\mathrm{xy}}$")
# plt.savefig(EigenVecName3)
# plt.close()

# EigenVecName4 = str(figPath + "EigVecs_cxy_v.pdf")
# plt.plot(Tb, Vxy[0], "b-o", linewidth=2, label=r"1$^{\mathrm{st}}$ Eigenvector for C$_{\mathrm{xy}}$ C'$_{\mathrm{xy}}$", alpha=0.65)
# plt.plot(Tb, Vxy[1], "r-o", linewidth=2, label=r"2$^{\mathrm{nd}}$ Eigenvector for C$_{\mathrm{xy}}$ C'$_{\mathrm{xy}}$", alpha=0.65)
# plt.plot(Tb, Vxy[2], "y-o", linewidth=2, label=r"3$^{\mathrm{rd}}$ Eigenvector for C$_{\mathrm{xy}}$ C'$_{\mathrm{xy}}$", alpha=0.65)
# plt.xticks(Tb, labels)
# plt.xlabel(r"Channel")
# plt.ylabel(r"Eigenvector")
# plt.xlim(0, 6)
# plt.legend(loc="best")
# plt.title(r"First three eigenvectors for C$_{\mathrm{xy}}$ C'$_{\mathrm{xy}}$")
# plt.savefig(EigenVecName4)
# plt.close()


#---------------------------------- RADAR PLOTS ----------------------------------#
# height = np.arange(20, 0, -0.250)
# radar = np.zeros(shape=(80,1))

# Plot all vertical radar echoes.
# for j in range(0, colSizeYRain):
# 	radar = rainyY[5:85 , j]
# 	name = str("radar_" + str(j))
# 	outputFiles = str(outputPath + name + ".pdf")
# 	plt.plot(radar, height)
#	plt.xlim(12, 60)
# 	plt.xlabel(r'Radar reflectivity (dbZ)')
# 	plt.ylabel(r'Height (km)')	
# 	plt.savefig(outputFiles)
# 	plt.close()
#
# Plot radar for a particular instance
# radar = rainyY[5:85, 100]
# plt.plot(radar, height)
#	plt.xlim(12, 60)
# plt.xlabel(r'Radar reflectivity (dbZ)')
# plt.ylabel(r'Height (km)')
# # plt.savefig(outputFiles)
# plt.savefig("./figs/radar_example.pdf")
# plt.close()


#---------------------------------- SCATTER PLOTS ----------------------------------#
# Scatter plot of the EigenVectors
EigenVecName = str(outputPath + "PCscores1_2_r.eps")
plt.scatter(pcScore1_r, pcScore2_r, s=10, facecolors='none', edgecolors='blue')
plt.savefig(EigenVecName)
plt.close()

# Scatter plot of brightness temperatures
# outScatterName = str(outputPath + scatter_name)
# plt.scatter(Tb4, Tb5)
# plt.xlabel(r"$T_{b}$ for channel 4")
# plt.ylabel(r"$T_{b}$ for channel 5")
# plt.xlim(200, 300)
# plt.title(r"Brightness temperatures from the AMSU-B radiometer")
# plt.savefig(outScatterName)
# plt.close()

# Scatter plot of brightness temps with variable of choice
# outScatterName2 = str(outputPath + "ice_Tb4_" + str(lev) + ".pdf")
# plt.scatter(var, Tb4)
# plt.xlim(-1, 20)
# plt.ylim(200, 290)
# plt.xlabel(r"Ice Water Content")
# plt.ylabel(r"$T_{b}$ for channel 4")
# title_str = str("Scatter plot of ice water content vs. brightness temperatures for level " + str(height_from_lev) + "m")
# plt.title(title_str, fontsize=10)
# plt.savefig(outScatterName2)
# plt.close()

# Scatter plot of total averaged columnar information vs brightness temperatures
# outScatterName3 = str(outputPath + "iwc_total_vs_Tb3.pdf")
# plt.scatter(T, Tb3)
# plt.xlim(0, 2)
# plt.ylim(200, 300)
# plt.xlabel(r"Total Ice Water Content")
# plt.ylabel(r"T$_\mathrm{b}$ for channel 3")
# plt.title(r"Total Ice Water Content vs. Brightness Temperaturees at 183 GHz")
# plt.savefig(outScatterName3)
# plt.close()

# Scatter plot of brightness temperature scores
# c = rainyH[:]
# outScatterName4 = str(outputPath + "PC1_2_scatt_clear.pdf")
# fig, ax = plt.subplots()
# im = ax.scatter(pcScore1_c, pcScore2_c, c=c, cmap=plt.cm.jet_r, edgecolors='none', alpha=0.75)
# cb = fig.colorbar(im, ax=ax)
# cb.set_label('Height of top of cloud (km)')
# plt.xlabel('1$^{\mathrm{st}}$ Principal Component Score')
# plt.ylabel('2$^{\mathrm{nd}}$ Principal Component Score')
# plt.xlim(-9, 3)
# plt.ylim(-3, 2)
# plt.savefig(outScatterName4)
# plt.close()


#---------------------------------- HISTOGRAMS ----------------------------------#
# histo_name = str(outputPath + "histo.pdf")
# var[var < 0] = 0
# X = np.ma.masked_equal(var, 0)
# sz = X.shape
# Y = np.reshape(X, sz)
# [n, bins, patches] = P.hist(X)
# plt.savefig(histo_name)
# plt.close()

# histo_name2 = str(outputPath + "histo2d.pdf")
# heatmap, xedges, yedges = np.histogram2d(rainyX[:], rainyY[:], bins=50)
# extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
# plt.clf()
# plt.imshow(heatmap, extent=extent)
# plt.savefig(histo_name2)
# plt.close()

#---------------------------------- OLD CODE ----------------------------------#
# print "Number of rows in Y structures for rain cases ", iSizeYRain
# print "Number of columns in Y structures for rain cases ", jSizeYRain
# print "Number of rows in X structure for rain cases ", iSizeXRain
# print "Number of columns in X structure for rain cases ", jSizeXRain
# print "Number of rows in Y structure for clear cases ", iSizeYClear
# print "Number of columns in Y structure for clear cases ", jSizeYClear
# print "Number of rows in X structure for clear cases ", iSizeXClear
# print "Number of columns in X structure for clear cases ", jSizeXClear

# print rainyY[0][0]

# with open(yRainData) as f1:
# 	reader = csv.reader(f1, delimiter=' ')
# 	first_row = next(reader)
# 	y_rain_num_cols = len(first_row)

# with open(yClearData) as f2:
# 	reader = csv.reader(f2, delimiter=' ')
# 	first_row = next(reader)
# 	y_clear_num_cols = len(first_row)

# with open(xRainData) as f3:
# 	reader = csv.reader(f3, delimiter=' ')
# 	first_row = next(reader)
# 	x_rain_num_cols = len(first_row)

# with open(xClearData) as f4:
# 	reader = csv.reader(f4, delimiter=' ')
# 	first_row = next(reader)
# 	x_clear_num_cols = len(first_row)

# print "Number of columns in Y and X for rainy cases (should be the same): "
# print y_rain_num_cols
# print x_rain_num_cols
# print "Number of columns in Y and X for clear cases (should be the same): "
# print y_clear_num_cols
# print x_clear_num_cols



# radar = rainyY[5:85,1]
# print radar.shape

# a = np.where(radar>0)
# b = a[0]

