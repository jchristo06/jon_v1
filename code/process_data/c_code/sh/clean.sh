#!/bin/bash
#
#	Clean the directories where the data is stored:
# 	../../../../files/X/$VAR/
# 	../../../../files/Y/$VAR/
# 	../../../../files/X_land/$VAR/
# 	../../../../files/Y_land/$VAR/
#	../../../../files/heights/$VAR/
#

echo " Cleaning Y folder "
rm ../../../../files/Y/rain/*.ascii
rm ../../../../files/Y/ice/*.ascii
rm ../../../../files/Y/mix/*.ascii
rm ../../../../files/Y/clear/*.ascii

echo " Cleaning X folder "
rm ../../../../files/X/rain/*.ascii
rm ../../../../files/X/ice/*.ascii
rm ../../../../files/X/mix/*.ascii
rm ../../../../files/X/clear/*.ascii

echo " Cleaning Y land folder "
rm ../../../../files/Y_land/rain/*.ascii
rm ../../../../files/Y_land/ice/*.ascii
rm ../../../../files/Y_land/mix/*.ascii
rm ../../../../files/Y_land/clear/*.ascii

echo " Cleaning X land folder "
rm ../../../../files/X_land/rain/*.ascii
rm ../../../../files/X_land/ice/*.ascii
rm ../../../../files/X_land/mix/*.ascii
rm ../../../../files/X_land/clear/*.ascii

echo " Cleaning Height folder "
rm ../../../../files/height/rain/*.ascii
rm ../../../../files/height/ice/*.ascii
rm ../../../../files/height/mix/*.ascii
rm ../../../../files/height/clear/*.ascii

echo " Cleaning ../cpp/ folder "
rm ../cpp/*.exe
rm ../cpp/*.out
rm filelist
rm ../cpp/filelist
